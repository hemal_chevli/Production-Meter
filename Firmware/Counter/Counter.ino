//arduino 1.0.6
//ADD lcd display
#include <EEPROM.h>
//#include <TimerOne.h>

#define PULSESPERMETER    1 //how many pulses from sensor equal 1 meter of cloth
#define CPMTHRESHOLD       8000  // GPM will reset after this many MS if no pulses are registered
#define INTERRUPTPIN   2  // INT4 PE4 pin 6
#define LED            13 // Moteinos have LEDs on D9
#define XMITPERIOD         5000  // GPMthreshold should be less than 2*XMITPERIOD
#define NOT_AN_INTERRUPT -1 // silent the compiler on 1.0.6

volatile byte ledState = LOW;
volatile unsigned long PulseCounterVolatile = 0; // use volatile for shared variables

unsigned long NOW = 0;
unsigned long PulseCounter = 0;
unsigned long LASTMINUTEMARK = 0;
unsigned long PULSECOUNTLASTMINUTEMARK = 0; //keeps pulse count at the last minute mark

byte COUNTEREEPROMSLOTS = 10;
unsigned long COUNTERADDRBASE = 8; //address in EEPROM that points to the first possible slot for a counter
unsigned long COUNTERADDR = 0;     //address in EEPROM that points to the latest Counter in EEPROM
byte secondCounter = 0;

unsigned long TIMESTAMP_pulse_prev = 0;
unsigned long TIMESTAMP_pulse_curr = 0;
int pulseAVGInterval = 0;
int pulsesPerXMITperiod = 0;
float CPM=0, CLM=0, METERS=0, METERSlast=0, CPMlast=0, CLMlast=0;
//CPM:cloth per minute
//CLM:cloth last minute
char buff[80];
char* METERstr="99999999999999.99"; //longest expected GAL message
char* CPMstr="99999.99"; //longest expected GPM message
char* CLMstr="9999999.99"; //longest expected GLM message

//int INTERRUPTPIN = 2;

void XMIT();

void setup() {
  Serial.begin(115200);
  unsigned long savedCounter = EEPROM_Read_Counter();
  if (savedCounter <=0) savedCounter = 1; //avoid division by 0

  PulseCounterVolatile = PulseCounter = PULSECOUNTLASTMINUTEMARK = savedCounter;
  attachInterrupt(digitalPinToInterrupt(INTERRUPTPIN), pulseCounterInterrupt, RISING);
  //attachInterrupt(INTERRUPTPIN, pulseCounterInterrupt, RISING);
}

void loop() {
  XMIT();
  Serial.println(METERS);
  delay(1000);
  /*
  if cloth not detected disable interrupts
  */

}

void pulseCounterInterrupt()
{
  noInterrupts();
  ledState = !ledState;
  PulseCounterVolatile++;  // increase when LED turns on
  digitalWrite(LED, ledState);
  NOW = millis();

  //remember how long between pulses (sliding window)
  TIMESTAMP_pulse_prev = TIMESTAMP_pulse_curr;
  TIMESTAMP_pulse_curr = NOW;
  
  if (TIMESTAMP_pulse_curr - TIMESTAMP_pulse_prev > CPMTHRESHOLD)
    //more than 'GPMthreshold' seconds passed since last pulse... resetting GPM
    pulsesPerXMITperiod=pulseAVGInterval=0;
  else
  {
    pulsesPerXMITperiod++;
    pulseAVGInterval += TIMESTAMP_pulse_curr - TIMESTAMP_pulse_prev;
  }
  interrupts();
}

void XMIT(){

  noInterrupts();
  PulseCounter = PulseCounterVolatile;
  interrupts();
  
  if (millis() - TIMESTAMP_pulse_curr >= 5000)
  {
    ledState = !ledState;
    digitalWrite(LED, ledState);
  }
  
  //calculate Gallons counter 
  METERS = ((float)PulseCounter)/PULSESPERMETER;
  Serial.print("PulseCounter:");Serial.print(PulseCounter);Serial.print(", Meter: "); Serial.print(METERS);

  //calculate & output GPM
  CPM = pulseAVGInterval > 0 ? 60.0 * 1000 * (1.0/PULSESPERMETER)/(pulseAVGInterval/pulsesPerXMITperiod)
                             : 0;
  dtostrf(METERS,3,2, METERstr);
  dtostrf(CPM,3,2, CPMstr);

  pulsesPerXMITperiod = 0;
  pulseAVGInterval = 0;
  secondCounter += XMITPERIOD/1000;

  //once per minute, output a GallonsLastMinute count
  if (secondCounter>=60)
  {
    //Serial.print("60sec mark ... ");
    secondCounter=0;
    CLM = ((float)(PulseCounter - PULSECOUNTLASTMINUTEMARK))/PULSESPERMETER;
    PULSECOUNTLASTMINUTEMARK = PulseCounter;
    EEPROM_Write_Counter(PulseCounter);
    dtostrf(CLM,3,2, CLMstr);
    sprintf(buff, "Meter:%s CPM:%s CLM:%s", METERstr, CPMstr, CLMstr);
   
  }
  else
  {
    sprintf(buff, "METER:%s CPM:%s", METERstr, CPMstr);    
  }

  if (CPM!=CPMlast || METERS !=METERSlast || CLM!=CLMlast)
  {
    //sendLen = strlen(buff);
    //radio.sendWithRetry(GATEWAYID, buff, sendLen);
    //Send data 
    METERSlast = METERS;
    CPMlast = CPM;
    CLMlast = CLM;
  }
  Serial.print(buff);
}

unsigned long EEPROM_Read_Counter()
{
  return EEPROM_Read_ULong(EEPROM_Read_ULong(COUNTERADDR));
}

void EEPROM_Write_Counter(unsigned long counterNow)
{
  if (counterNow == EEPROM_Read_Counter())
  {
    Serial.print("{EEPROM-SKIP(no changes)}");
    return; //skip if nothing changed
  }
  
  Serial.print("{EEPROM-SAVE(");
  Serial.print(EEPROM_Read_ULong(COUNTERADDR));
  Serial.print(")=");
  Serial.print(PulseCounter);
  Serial.print("}");
    
  unsigned long CounterAddr = EEPROM_Read_ULong(COUNTERADDR);
  if (CounterAddr == COUNTERADDRBASE+8*(COUNTEREEPROMSLOTS-1))
    CounterAddr = COUNTERADDRBASE;
  else CounterAddr += 8;
  
  EEPROM_Write_ULong(CounterAddr, counterNow);
  EEPROM_Write_ULong(COUNTERADDR, CounterAddr);
}

unsigned long EEPROM_Read_ULong(int address)
{
  unsigned long temp;
  for (byte i=0; i<8; i++)
    temp = (temp << 8) + EEPROM.read(address++);
  return temp;
}

void EEPROM_Write_ULong(int address, unsigned long data)
{
  for (byte i=0; i<8; i++)
  {
    EEPROM.write(address+7-i, data);
    data = data >> 8;
  }
}
