//Production Meter8IN1
//Date:18-MAR-16
//arduino 1.0.6

//TODO
//Hangs with busy p... botch and do hard reset, use debug pins
//Get latest value from server use json Testing
//save counter on update only
//cloth calibration routine on cycle button press

/*
Things to update at installation
  FORMAT sd CARD
  Run SetRTC with full compailation
  Run clearSD and eeprom program
  SET AT+CWMODE=1
  join wifi network
  CIPStart post host
  DeviceID
  API key
  change Shift time
  Pulsespermeter
  CPM threshold time
  clothThreshold
  remove debugging true in setup
*/

#include <avr/wdt.h>
#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <SD.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <EnableInterrupt.h>

#define CycleButton 2

bool netOK = false;     //checks internet is alive

const unsigned int DataupdateFreq = 30000; //update every 30s
const unsigned long DataupdateFreq_SD = 600000; //update every 10 minutes on SD(144 entries per 24 hrs)

const unsigned int lcdCycleDelay = 10000;
unsigned long previousMillis = 0; //for lcd cycle
int lcdState = 0;

bool debugging = false; //if password is ok, this is true
String debugPass = String("amethyst");
String str; //for storing input password

Sd2Card card;
SdVolume volume;
SdFile root;
File Datalog;
const int chipSelect = 53;

HardwareSerial & debug    = Serial;
HardwareSerial & esp8266  = Serial1;

const char DeviceID[] PROGMEM = "Blk1";
const char APIKey[]PROGMEM = "6359147591";

const char* const Device_API[] PROGMEM = {DeviceID,APIKey};

//Use GMT time(2:30AM -> 8:00AM)
const int ShiftStartHour = 2;
const int ShiftStartMinute = 30 + 5 ;
//14:30 ->20:00(8:00PM)
const int ShiftEndHour = 14;
const int ShiftEndMinute = 30 + 5 ;

int shiftNow=0;
int preShift=0;
int eepromShift;
const int eepromShiftAdd = 64;

//AT commands stored in flash
const char at[] PROGMEM=            "AT";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char rst[] PROGMEM =          "AT+RST";
const char cwjap[] PROGMEM =        "AT+CWJAP=\"ProductionMeter\",\"blackelectronics@123\"";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"blackelectronic.cloudapp.net\",8081";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";
const char cwdhcp[] PROGMEM =       "AT+CWDHCP=1,1";
const char staticIP[] PROGMEM =     "AT+CIPSTA=\"192.168.1.124\"";

const char POST[] PROGMEM =           "POST /api/service/InsertLog HTTP/1.1";
const char HOST[] PROGMEM =           "Host: blackelectronic.cloudapp.net:8081";
const char contentType[] PROGMEM =    "Content-Type: application/json";
const char contentLength[] PROGMEM =  "Content-Length: ";

//pointers to AT commands
const char*  AT PROGMEM = at;
const char*  CIPSTATUS PROGMEM = cipstatus;
const char*  RESET PROGMEM = rst;
const char*  CWJAP PROGMEM = cwjap;
const char*  CIPSTART PROGMEM = cipstart;
const char*  CIPCLOSE PROGMEM = cipclose;
const char*  CIFSR PROGMEM = cifsr;
const char*  CIPMUX PROGMEM = cipmux;
const char*  CWDHCP PROGMEM = cwdhcp;
const char*  STATICIP PROGMEM = staticIP;
const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};

char cmdBuffer[100];
int postlen = 0;
int connection;

//Date Time
tmElements_t tmx;
bool RTCUpdatedFlag= false; //true updated
int Restarted = 1;  //RTC update on each reboot
int getTimefromNW = 0;
unsigned long sentTime; //use for various waiting while loops

//For RTC update, updates every month from nw
int NewMonth;
int Month;

//For eeprom save each hour
int NewHour, Hour;

int fileSent = 1; //0 not sent, 1 sent
int cipstartStatus;//main

unsigned long entries; //entries in SD log file
unsigned long sent_entries; //

bool transfer_in_progress = false;

const int openDetect = 3;
bool BoxOpen = false;

bool SDcardPresent = false; //Access sd fuction if this is true
const int CardDetect =49;

const int RS = 12;
const int E  = 11;
const int D4 = 10;
const int D5 = 9;
const int D6 = 8;
const int D7 = 7;

LiquidCrystal lcd(RS,E,D4,D5,D6,D7);

struct Proxy
{
  const int ProxyPin;
  const int counterEEpromAddress;int pulsePerMeter;
  volatile unsigned long PulseCounterVolatile;
  volatile float Meter;
  float preMeters;
  unsigned long PulseCounter;

};
struct ClothSensor
{
  const int sensorPin;
  bool clothPresent;
  int clothThreshold;
  int clothReading;
};

struct Proxy P1 = {A8, 0};
struct Proxy P2 = {A9, 8};
struct Proxy P3 = {A10,16};
struct Proxy P4 = {A11,24};
struct Proxy P5 = {A12,32};
struct Proxy P6 = {A13,40};
struct Proxy P7 = {A14,48};
struct Proxy P8 = {A15,56};

struct ClothSensor S1 = {A0};
struct ClothSensor S2 = {A1};
struct ClothSensor S3 = {A2};
struct ClothSensor S4 = {A3};
struct ClothSensor S5 = {A4};
struct ClothSensor S6 = {A5};
struct ClothSensor S7 = {A6};
struct ClothSensor S8 = {A7};

void setup(){

S1.clothPresent = true;
S1.clothThreshold = 200;
S1.clothReading = 0;

S2.clothPresent = true;
S2.clothThreshold = 200;
S2.clothReading = 0;

S3.clothPresent = true;
S3.clothThreshold = 200;
S3.clothReading = 0;

S4.clothPresent = true;
S4.clothThreshold = 200;
S4.clothReading = 0;


S5.clothPresent = true;
S5.clothThreshold = 200;
S5.clothReading = 0;


S6.clothPresent = true;
S6.clothThreshold = 200;
S6.clothReading = 0;


S7.clothPresent = true;
S7.clothThreshold = 200;
S7.clothReading = 0;


S8.clothPresent = true;
S8.clothThreshold = 200;
S8.clothReading = 0;

///////////////////////////


P1.pulsePerMeter = 1;
P1.PulseCounterVolatile = 0;
P1.Meter = 0;
P1.preMeters = 0;
P1.PulseCounter = 0;

P2.pulsePerMeter = 1;
P2.PulseCounterVolatile = 0;
P2.Meter = 0;
P2.preMeters = 0;
P2.PulseCounter = 0;

P3.pulsePerMeter = 1;
P3.PulseCounterVolatile = 0;
P3.Meter = 0;
P3.preMeters = 0;
P3.PulseCounter = 0;

P4.pulsePerMeter = 1;
P4.PulseCounterVolatile = 0;
P4.Meter = 0;
P4.preMeters = 0;
P4.PulseCounter = 0;

P5.pulsePerMeter = 1;
P5.PulseCounterVolatile = 0;
P5.Meter = 0;
P5.preMeters = 0;
P5.PulseCounter = 0;


P6.pulsePerMeter = 1;
P6.PulseCounterVolatile = 0;
P6.Meter = 0;
P6.preMeters = 0;
P6.PulseCounter = 0;


P7.pulsePerMeter = 1;
P7.PulseCounterVolatile = 0;
P7.Meter = 0;
P7.preMeters = 0;
P7.PulseCounter = 0;

P8.pulsePerMeter = 1;
P8.PulseCounterVolatile = 0;
P8.Meter = 0;
P8.preMeters = 0;
P8.PulseCounter = 0;
  ////////////////////////
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("BlackElectronics");
  lcd.setCursor(0,1);
  lcd.print("WiFi Prod. Meter");

  pinMode(openDetect,INPUT);
  pinMode(CardDetect,INPUT_PULLUP);

  debug.begin(115200);
  esp8266.begin(115200);
  debug.println(F("---------------------"));
  debug.println(F("Black Electronics"));
  debug.println(F("WiFi Production Meter"));
  debug.println(F("---------------------"));
  debug.println(F("Please Enter Password:"));
  delay(3000);
  //Read password from user
  if(debug.available()>0){
    while(debug.available()){
      str = Serial.readStringUntil('\n');
    }
  }
  str.trim();//removes \n
  if(str == debugPass){ //
    debugging = true;
    debug.println(F("ok"));
  }
  else{
    debugging = true;
    debug.println(F("Timeout"));
  }
  lcd.clear();
  //SD setup
  pinMode(chipSelect, OUTPUT); //53 is chip select
  if (!SD.begin(chipSelect)) {
    if(debugging) debug.println(F("\r\nInitialization failed!"));
    SDcardPresent = false;
  }
  else{
    if(debugging) debug.println(F("\r\nInitialization done."));
    SDcardPresent = true;
  }

  //ESP setup
  postlen = sizeof(POST) + sizeof(HOST) + sizeof(contentType) + sizeof(contentLength) + 7; //calcuate number for cipsend
  sendATcommand("AT","OK",1000,debugging);
  //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIFSR)));
  //sendATcommand(cmdBuffer,"OK",3000,debugging);

  //read out contents of SD card
  if(SD.exists("LOG.TXT")){
    fileSent = 0; //File not sent flag
    Datalog = SD.open("LOG.TXT");
    lcd.setCursor(0,0);
    lcd.print("Reading SD");
    //lcd reading SD card
    while(Datalog.available()){
      if (Datalog.read() == '\r'){ //or entries more than x amt break
        entries++; //count jsons entries not sent
        }
    }
    if(debugging){
      debug.print(F("No. of Entries: "));debug.println(entries);
    }
    entries = 0;
    Datalog.close();
  }

  ReloadCountersEEPROM();

  enableInterrupt(P1.ProxyPin, pulseCounterInterrupt1, FALLING);
  enableInterrupt(P2.ProxyPin, pulseCounterInterrupt2, FALLING);
  enableInterrupt(P3.ProxyPin, pulseCounterInterrupt3, FALLING);
  enableInterrupt(P4.ProxyPin, pulseCounterInterrupt4, FALLING);
  enableInterrupt(P5.ProxyPin, pulseCounterInterrupt5, FALLING);
  enableInterrupt(P6.ProxyPin, pulseCounterInterrupt6, FALLING);
  enableInterrupt(P7.ProxyPin, pulseCounterInterrupt7, FALLING);
  enableInterrupt(P8.ProxyPin, pulseCounterInterrupt8, FALLING);

  enableInterrupt(CycleButton, cycleValues, FALLING);

  RTC.read(tmx);
  NewMonth = Month = tmx.Month;
  NewHour = Hour = tmx.Hour;
  //Determine which shift we are in
  //if(tmx.Hour <= ShiftHour && tmx.Minute <ShiftMinute){
  if(ShiftStartHour < tmx.Hour && tmx.Hour < ShiftEndHour){
    shiftNow = 1;
    preShift = 1;
  }
  else{
    shiftNow = 2;
    preShift = 2;
  }
  //read eeprom for shift data
  eepromShift = EEPROM.read(eepromShiftAdd);
  if(eepromShift != preShift){
    preShift = eepromShift;
    if(debugging) debug.println("---Shift changed");
  }
  if(debugging) debug.print(F("Shift: "));debug.println(shiftNow);
  //Shift times
  if(debugging) {
    sprintf(cmdBuffer,"Shift Time: %d:%d(GMT)",ShiftStartHour,ShiftStartMinute);
    debug.println(cmdBuffer);
    debug.print(F("DeviceID: "));
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[0])));
    debug.println(cmdBuffer);
  }
}////////////////////////////////////////////////////setup end

void loop(){
  //cycle button press
  //start calibration of analog sensors
  //take average values
  //store in eeprom
  updateLCD();
  //Determine the shift
  if(ShiftStartHour <= tmx.Hour  && tmx.Hour < ShiftEndHour){
    if(shiftNow != 1 && tmx.Minute ==30){
      shiftNow = 1;
      EEPROM.write(eepromShiftAdd,shiftNow);
      debug.println("Shift written on eeprom");
    }
  }
  else{
    if(shiftNow != 2 && tmx.Minute ==30){
      shiftNow = 2;
      EEPROM.write(eepromShiftAdd,shiftNow);
      debug.println("Shift written on eeprom");
    }
  }
  //if shift has changed reset all counters to zero
  if(shiftNow != preShift){
    debug.println("---Shift changed");
    preShift = shiftNow;
    noInterrupts();
    // Reset counter to zero at shift start
    P1.Meter = 0;
    P1.PulseCounterVolatile = 0;
    P1.PulseCounter = 0;

    P2.Meter = 0;
    P2.PulseCounterVolatile = 0;
    P2.PulseCounter = 0;

    P3.Meter = 0;
    P3.PulseCounterVolatile = 0;
    P3.PulseCounter = 0;

    P4.Meter = 0;
    P4.PulseCounterVolatile = 0;
    P4.PulseCounter = 0;

    P5.Meter = 0;
    P5.PulseCounterVolatile = 0;
    P5.PulseCounter = 0;

    P6.Meter = 0;
    P6.PulseCounterVolatile = 0;
    P6.PulseCounter = 0;

    P7.Meter = 0;
    P7.PulseCounterVolatile = 0;
    P7.PulseCounter = 0;

    P8.Meter = 0;
    P8.PulseCounterVolatile = 0;
    P8.PulseCounter = 0;

    SaveCountersEEPROM();
    interrupts();
  }
  //Save counter every Hour
  NewHour = tmx.Hour;
  if(NewHour != Hour){
    Hour = NewHour;
    debug.println(); //New line for eeprom msg
    SaveCountersEEPROM();
  }
  //update rtc every month and every reboot
  NewMonth = tmx.Month;
  if( (NewMonth != Month && netOK)  || (Restarted && netOK)){
    Month = NewMonth;
    Restarted = 0;
    getTimefromNW = 1;
    RTCUpdatedFlag = false;
    if(debugging) debug.println(F("\r\nRefreshing RTC..."));
  }
  //Read and send Data
  //check if data is updated
  connection = connStatus();  //Get cipstatus
  switch (connection) {
    case 2: case 4: //connected to AP and HAS IP
      //DO TCP START
      if(debugging)debug.println(F("Connected to AP and has IP"));
      //close previous connection
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE)));
      sendATcommand(cmdBuffer,"OK",2000,debugging);
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART)));
      cipstartStatus = sendATcommand(cmdBuffer,"OK",7000,false);
      switch (cipstartStatus){
        case 1: //OK
          netOK = true;
        break;
        case 2:  case 3://DNS fail
          debug.println(F("Internet/Router Down"));
          netOK = false;
          debug.println("Wait for 10mins");
          //if any counter is updated write to sd card
          if(
            P1.Meter != P1.preMeters || P5.Meter != P5.preMeters ||
            P2.Meter != P2.preMeters || P6.Meter != P6.preMeters ||
            P3.Meter != P3.preMeters || P7.Meter != P7.preMeters ||
            P4.Meter != P4.preMeters || P8.Meter != P8.preMeters && SDcardPresent){

            P1.preMeters= P1.Meter;
            P2.preMeters= P2.Meter;
            P3.preMeters= P3.Meter;
            P4.preMeters= P4.Meter;
            P5.preMeters= P5.Meter;
            P6.preMeters= P6.Meter;
            P7.preMeters= P7.Meter;
            P8.preMeters= P8.Meter;

            save2SD();
          }
          sentTime = millis();
          while(millis()-sentTime < DataupdateFreq_SD){
            updateLCD();
          }
        break;
      }//CIPStartstate switch end
      break; //Case2/4 break of cipstatus
    case 3: //Connected to server, send post request
      if(getTimefromNW == 1){
        sendData(getTimefromNW);
        getTimefromNW = 0;
        strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE)));
        sendATcommand(cmdBuffer,"OK",2000,debugging);
        sentTime = millis();
        while(millis()-sentTime<DataupdateFreq){
          updateLCD(); //read status while waiting for timout
        }
      }
      else{
        //send current data
        sendData(0);
        if(fileSent == 0){//file not sent
          sentTime = millis();
          //while(millis()-sentTime<DataupdateFreq || (SD.exists("LOG.TXT"))) {
          while(millis()-sentTime<DataupdateFreq && (SD.exists("LOG.TXT"))) {
            updateLCD();  //read inputs
            if(fileSent == 0){
              int connection_SD = connStatus();
              switch(connection_SD){
                case 2: case 4:
                  debug.println(F("CASE:2 or 4 connected to AP and has IP"));
                  //close previous connection
                  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE)));
                  sendATcommand(cmdBuffer,"OK",2000,debugging);
                  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART)));
                  sendATcommand(cmdBuffer,"OK",7000,false);
                break;
                case 3:
                  if(SDcardPresent)
                    sendfromSD();
                  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE)));
                  sendATcommand(cmdBuffer,"OK",2000,debugging);
                break;
              }
            }
          }//while loop end
        }
        else{ //if file is sent wait
          sentTime = millis();
          while(millis()-sentTime<DataupdateFreq){
           updateLCD();
          }
        }
      }//case 3 end
      break;
    case 5:
      debug.println(F("CASE 5: Not connected to AP"));
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CWJAP)));
      sendATcommand(cmdBuffer,"OK",5000,debugging);
      break;
    default:
    //close connection
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE)));
      sendATcommand(cmdBuffer,"OK",2000,debugging);
      break;
  }//switch end
}//loop end
//////////////////////////////////////////////////////////

///Functions
void updateLCD(void){
//updates lcd and reads cloth sensor
  if(tmx.Year <=40 || tmx.Year >= 70){ //Time glitch work around
    Restarted = 1; //update again
  }
  BoxOpen = !(digitalRead(openDetect)); //0 for closed, 1 open
  //If not using cloth sensor

  S1.clothReading = 1024;
  S2.clothReading = 1024;
  S3.clothReading = 1024;
  S4.clothReading = 1024;
  S5.clothReading = 1024;
  S6.clothReading = 1024;
  S7.clothReading = 1024;
  S8.clothReading = 1024;

/*
  clothReading1 = analogRead(ClothSensorPin1);
  clothReading2 = analogRead(ClothSensorPin2);
  clothReading3 = analogRead(ClothSensorPin3);
  clothReading4 = analogRead(ClothSensorPin4);
  clothReading5 = analogRead(ClothSensorPin5);
  clothReading6 = analogRead(ClothSensorPin6);
  clothReading7 = analogRead(ClothSensorPin7);
  clothReading8 = analogRead(ClothSensorPin8);
*/
  S1.clothPresent = S1.clothReading >= S1.clothThreshold ? true : false;
  S2.clothPresent = S2.clothReading >= S2.clothThreshold ? true : false;
  S3.clothPresent = S3.clothReading >= S3.clothThreshold ? true : false;
  S4.clothPresent = S4.clothReading >= S4.clothThreshold ? true : false;
  S5.clothPresent = S5.clothReading >= S5.clothThreshold ? true : false;
  S6.clothPresent = S6.clothReading >= S6.clothThreshold ? true : false;
  S7.clothPresent = S7.clothReading >= S7.clothThreshold ? true : false;
  S8.clothPresent = S8.clothReading >= S8.clothThreshold ? true : false;

  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis >= lcdCycleDelay){
    previousMillis = currentMillis;
    if (lcdState == 0){
      lcdState = 1;
      lcd.clear();
    }
    else{
      lcdState = 0;
      lcd.clear();
    }
  }
  if(lcdState == 0){
    lcd.setCursor(0,0);
    lcd.print("1:");lcd.print((int)P1.Meter);lcd.print(",3:");lcd.print((int)P3.Meter);
    lcd.setCursor(0,1);
    lcd.print("2:");lcd.print((int)P2.Meter);lcd.print(",4:");lcd.print((int)P4.Meter);
  }
  else {
    lcd.setCursor(0,0);
    lcd.print("5:");lcd.print((int)P5.Meter);lcd.print(",7:");lcd.print((int)P7.Meter);
    lcd.setCursor(0,1);
    lcd.print("6:");lcd.print((int)P6.Meter);lcd.print(",8:");lcd.print((int)P8.Meter);
  }
}
void sendfromSD(void){
  debug.println(F("\r\nSending from SD"));
  if(!transfer_in_progress){
    if(SD.exists("LOG.TXT")){
      Datalog = SD.open("LOG.TXT");
      Datalog.seek(0);
      while(Datalog.available()){
        if (Datalog.read() == '\r'){ //start of new dataline
          entries++; //count jsons
        }
      }
      debug.print(F("\r\nTotal Entries on SD: "));debug.println(entries);
    Datalog.seek(0); //go back to begining of file or close and reopen
    }
    transfer_in_progress = true;

  }
  //check eeprom for sent entries
  //copy to sententries variable in RAM
  //if present run for loop to jump to appropriate location to send
  if (sent_entries < entries){
    sendData(2);  //2 reads json from log file
    sent_entries++;
    debug.print(F("\r\nSent Entries: "));debug.println(sent_entries);
    //write sent entries in eeprom
   // EEPROMWritelong(1,sent_entries);
  }
  else if(sent_entries >= entries && fileSent == 0 ){
    //clean up variables
    debug.println(F("\r\nLog file sent and removed"));
    fileSent = 1;
    transfer_in_progress = false;
    entries = 0;
    sent_entries = 0;
    SD.remove("LOG.TXT");
  }
}

void save2SD(void){
  fileSent = 0;
  Datalog = SD.open("LOG.TXT",FILE_WRITE);//write or append
  if(Datalog){ //if file open
    String machine_data = createJSON();
    Datalog.println(machine_data); //write to file
    Datalog.close(); //close file to save the contents
    debug.println(F("\r\nData saved to SD"));
    if(debugging)debug.println(machine_data);
  }
  else
    debug.println(F("\r\nError opening file"));
    //Display on LCD to change SD card
}

byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
}

void sendData(int command){
  //command = 0 -> Normal send
  //command = 1 -> update RTC
  //command = 2 -> send from SD card
  char date_string[32];
  char *datep = &date_string[0];

  String json;
  int jsonLen; //length of json string
  int jsonlenlen; //jsonlen is 120, jsonlenlen is 3
  int cipsendLen; //total of cipsend

    if(command == 2){ //sending from SD card
      json = Datalog.readStringUntil('\n');
    }
    else{
      json = createJSON();
    }
    jsonLen = json.length();
    jsonlenlen = countDigits(jsonLen);
    cipsendLen = jsonLen + jsonlenlen + postlen ;
    //remove this.
    debug.println(json);
    sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    sendATcommand(cmdBuffer,">",1000,debugging);
    for (int i = 0; i <=2 ; i++){
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[i])));
      esp8266.println(cmdBuffer);
      delay(50);
    }
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3])));
    esp8266.print(cmdBuffer);
    esp8266.println(jsonLen);
    esp8266.println();
    delay(50);
    esp8266.print(json);
    sendATcommand(" ","SEND OK",5000,debugging); //consider removing this

  //Parse the header for date
  if(command == 1 && !(RTCUpdatedFlag)){
    //update only if data is available from the network
     //Parse the returned header & web page. Looking for 'Date' line in header
    if (esp8266.find("Date: ")) //get the date line from the http header (for example)
    {
      int i;
      for (i = 0; i < 31; i++) //31 this should capture the 'Date: ' line from the header
      {
        if (esp8266.available())  //new characters received?
        {
          char c = esp8266.read(); //print to console
          //debug.write(c);
          date_string[i] = c;
        }
        else i--;  //if not, keep going round loop until we've got all the characters
      }
    }
    string_to_tm(&tmx, date_string);
    //update RTC
    if(tmx.Year <= 70 && tmx.Year>=40){ //Time glitch work around
      if(RTC.write(tmx)){
        RTCUpdatedFlag = true;
        if(debugging) debug.println(F("\r\nRTC Updated"));
      }
    }
    if (RTC.read(tmx)) {
      if(debugging) {
        debug.print(F("Ok, Time = "));
        debug.print(tmx.Hour);
        debug.write(':');
        debug.print(tmx.Minute);
        debug.write(':');
        debug.print(tmx.Second);
        debug.print(F(", Date (D/M/Y) = "));
        debug.print(tmx.Day);
        debug.write('/');
        debug.print(tmx.Month);
        debug.write('/');
        debug.print(tmYearToCalendar(tmx.Year));
        debug.println();
      }
    }
  }//RTC update end
  /*if(debugging){
    debug.println();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[1]))); // Necessary casts and dereferencing, just copy.
    json.replace(cmdBuffer,"");
    json.replace("{","");
    json.replace("}","");
    json.replace("[","");
    json.replace("]","");
    json.replace(":","-");
    json.replace(":","-");
    json.replace(","," ");
    json.replace("\"","");

    json.replace("DeviceID-","");
    json.replace("Meters-","Param-");
    json.replace("APIKey-","");
    json.replace("BoxOpen","");
    debug.println(json);//change this
  }*/
}//send data end

int8_t connStatus(void){
    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTATUS)));
    esp8266.println(cmdBuffer); //send cipstatus command
    x = 0;
    previous = millis();
    // this loop waits for the answer
    do{
      if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
          response[x] = esp8266.read();
          if(debugging){
              debug.print(response[x]);
          }
          x++;
          if      (strstr(response, "STATUS:2") != NULL)    //connected to AP
          {
              answer = 2;
          }
          else if (strstr(response, "STATUS:3") != NULL)    //connected to webserver
          {
              answer = 3;
          }
          else if (strstr(response, "STATUS:4") != NULL)    //disconnected from webserver
          {
              answer = 4;
          }
          else if (strstr(response, "STATUS:5") != NULL)    //not connected to ap
          {
              answer = 5;
          }
      }
    }while((answer == 0) && ((millis() - previous) < 5000));    // Waits for the asnwer with time out

    return answer;
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command
    }
    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
      if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
          response[x] = esp8266.read();
          if(isDebug && debugging){
              debug.print(response[x]);
          }
          x++;
          if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 1;
          }
          else if(strstr(response, "DNS Fail") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 2;
              break;
          }
          else if(strstr(response, "no ip") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 3;
              break;
          }
          else if(strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 0;
              break;
          }
      }
    updateLCD();
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out
  return answer;
}

String createJSON(void){

  String json;
  updateLCD();
  RTC.read(tmx);
  json = "{";
  json += "\"DT\":\"";
  if(tmx.Day > 0 && tmx.Day <10){
    json += "0";
  }
  json += tmx.Day;
  json += "-";
  if(tmx.Month >0 && tmx.Month <10){
    json += "0";
  }
  json += tmx.Month;
  json += "-";
  json += tmYearToCalendar(tmx.Year);

  json += " ";
  if(tmx.Hour >=0 && tmx.Hour <10){
    json += "0";
  }
  json += tmx.Hour;
  json += ":";
  if(tmx.Minute >=0 && tmx.Minute <10){
    json += "0";
  }
  json += tmx.Minute;
  json += ":";
  if(tmx.Second >=0 && tmx.Second <10){
    json += "0";
  }
  json += tmx.Second;
  json +="\",\"DeviceID\":\"";
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[0]))); // Necessary casts and dereferencing, just copy.
  json += cmdBuffer; //device id
//  {"DT":"19-03-2016 06:33:03","DeviceID":"Blk1","Meters":[123.8,1.8235,125.3,12.3,125.3,15.23,123.6,123.6],"BoxOpen":0,"APIKey":"635914759"}
  json += "\",\"Meters\":[";
  //no interrupes
  json += P1.Meter;
  json += ",";
  json += P2.Meter;
  json += ",";
  json += P3.Meter;
  json += ",";
  json += P4.Meter;
  json += ",";
  json += P5.Meter;
  json += ",";
  json += P6.Meter;
  json += ",";
  json += P7.Meter;
  json += ",";
  json += P8.Meter;
  json += "]";
  //interrupts
  json += ",\"BoxOpen\":";
  json += BoxOpen;
  json += ",\"APIKey\":\"";
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[1]))); // Necessary casts and dereferencing, just copy.
  json += cmdBuffer; //APIkey
  json += "\"}";

  return json;
}

tmElements_t * string_to_tm(tmElements_t *tme, char *str) {
  // Sat, 28 Mar 2015 13:53:38 GMT

  const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  char *r, *i, *t;
  r = strtok_r(str, " ", &i);

  r = strtok_r(NULL, " ", &i);
  tme->Day = atoi(r);

  r = strtok_r(NULL, " ", &i);
  for (int i = 0; i < 12; i++) {
    if (!strcmp(months[i], r)) {
      tme->Month = i + 1;
      break;
    }
  }
  r = strtok_r(NULL, " ", &i);
  tme->Year = atoi(r) - 1970;

  r = strtok_r(NULL, " ", &i);
  t = strtok_r(r, ":", &i);
  tme->Hour = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Minute = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Second = atoi(t);

  return tme;
}

unsigned long EEPROM_Read_ULong(int address){
  unsigned long temp;
  for (byte i=0; i<8; i++)
    temp = (temp << 8) + EEPROM.read(address++);
  return temp;
}

void EEPROM_Write_ULong(int address, unsigned long data){
  for (byte i=0; i<8; i++)
  {
    EEPROM.write(address+7-i, data);
    data = data >> 8;
  }
}

void SaveCountersEEPROM(void){
  //read from eeprom, compare with current values
  //update counters whose value has changed
  EEPROM_Write_ULong(P1.counterEEpromAddress,P1.PulseCounter);
  EEPROM_Write_ULong(P2.counterEEpromAddress,P2.PulseCounter);
  EEPROM_Write_ULong(P3.counterEEpromAddress,P3.PulseCounter);
  EEPROM_Write_ULong(P4.counterEEpromAddress,P4.PulseCounter);
  EEPROM_Write_ULong(P5.counterEEpromAddress,P5.PulseCounter);
  EEPROM_Write_ULong(P6.counterEEpromAddress,P6.PulseCounter);
  EEPROM_Write_ULong(P7.counterEEpromAddress,P7.PulseCounter);
  EEPROM_Write_ULong(P8.counterEEpromAddress,P8.PulseCounter);
}
void ReloadCountersEEPROM(void){
  P1.PulseCounter = EEPROM_Read_ULong(P1.counterEEpromAddress);
  P2.PulseCounter = EEPROM_Read_ULong(P2.counterEEpromAddress);
  P3.PulseCounter = EEPROM_Read_ULong(P3.counterEEpromAddress);
  P4.PulseCounter = EEPROM_Read_ULong(P4.counterEEpromAddress);
  P5.PulseCounter = EEPROM_Read_ULong(P5.counterEEpromAddress);
  P6.PulseCounter = EEPROM_Read_ULong(P6.counterEEpromAddress);
  P7.PulseCounter = EEPROM_Read_ULong(P7.counterEEpromAddress);
  P8.PulseCounter = EEPROM_Read_ULong(P8.counterEEpromAddress);

  P1.PulseCounterVolatile = P1.PulseCounter;
  P2.PulseCounterVolatile = P2.PulseCounter;
  P3.PulseCounterVolatile = P3.PulseCounter;
  P4.PulseCounterVolatile = P4.PulseCounter;
  P5.PulseCounterVolatile = P5.PulseCounter;
  P6.PulseCounterVolatile = P6.PulseCounter;
  P7.PulseCounterVolatile = P7.PulseCounter;
  P8.PulseCounterVolatile = P8.PulseCounter;
  //Calulate Meters from saved Counter
  P1.Meter = ((float)P1.PulseCounter)/P1.pulsePerMeter;
  P2.Meter = ((float)P2.PulseCounter)/P2.pulsePerMeter;
  P3.Meter = ((float)P3.PulseCounter)/P3.pulsePerMeter;
  P4.Meter = ((float)P4.PulseCounter)/P4.pulsePerMeter;
  P5.Meter = ((float)P5.PulseCounter)/P5.pulsePerMeter;
  P6.Meter = ((float)P6.PulseCounter)/P6.pulsePerMeter;
  P7.Meter = ((float)P7.PulseCounter)/P7.pulsePerMeter;
  P8.Meter = ((float)P8.PulseCounter)/P8.pulsePerMeter;

}
/////////////////////////////
void pulseCounterInterrupt1(void)
{
  if(S1.clothPresent){
    P1.PulseCounterVolatile++;
  }

  P1.PulseCounter = P1.PulseCounterVolatile;
  P1.Meter = ((float)P1.PulseCounter)/P1.pulsePerMeter;
}
void pulseCounterInterrupt2(void)
{
  if(S2.clothPresent){
    P2.PulseCounterVolatile++;
  }

  P2.PulseCounter = P2.PulseCounterVolatile;
  P2.Meter = ((float)P2.PulseCounter)/P2.pulsePerMeter;
}
void pulseCounterInterrupt3(void)
{
  if(S3.clothPresent){
    P3.PulseCounterVolatile++;
  }

  P3.PulseCounter = P3.PulseCounterVolatile;
  P3.Meter = ((float)P3.PulseCounter)/P3.pulsePerMeter;
}
void pulseCounterInterrupt4(void)
{
  if(S4.clothPresent){
    P4.PulseCounterVolatile++;
  }

  P4.PulseCounter = P4.PulseCounterVolatile;
  P4.Meter = ((float)P4.PulseCounter)/P4.pulsePerMeter;
}
void pulseCounterInterrupt5(void)
{
  if(S5.clothPresent){
    P5.PulseCounterVolatile++;
  }

  P5.PulseCounter = P5.PulseCounterVolatile;
  P5.Meter = ((float)P5.PulseCounter)/P5.pulsePerMeter;
}
void pulseCounterInterrupt6(void)
{
  if(S6.clothPresent){
    P6.PulseCounterVolatile++;
  }

  P6.PulseCounter = P6.PulseCounterVolatile;
  P6.Meter = ((float)P6.PulseCounter)/P6.pulsePerMeter;
}
void pulseCounterInterrupt7(void)
{
  if(S7.clothPresent){
    P7.PulseCounterVolatile++;
  }

  P7.PulseCounter = P7.PulseCounterVolatile;
  P7.Meter = ((float)P7.PulseCounter)/P7.pulsePerMeter;
}
void pulseCounterInterrupt8(void)
{
  if(S8.clothPresent){
    P8.PulseCounterVolatile++;
  }

  P8.PulseCounter = P8.PulseCounterVolatile;
  P8.Meter = ((float)P8.PulseCounter)/P8.pulsePerMeter;
}

void cycleValues(void){
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200)
  {
    //do stuff
  }
  last_interrupt_time = interrupt_time;
}

/*
String createJSON(void);
void sendData(int updateRTC);
byte countDigits(int num);
tmElements_t * string_to_tm(tmElements_t *tme, char *str);
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug);
int8_t connStatus(void);
void save2SD(void);
void sendfromSD(void);
unsigned long EEPROM_Read_ULong(int address);
void EEPROM_Write_ULong(int address, unsigned long data);
void SaveCountersEEPROM(void);
void ReloadCountersEEPROM(void);
void updateLCD(void);
//interrupt funciton
void cycleValues(void);
void pulseCounterInterrupt1(void);
void pulseCounterInterrupt2(void);
void pulseCounterInterrupt3(void);
void pulseCounterInterrupt4(void);
void pulseCounterInterrupt5(void);
void pulseCounterInterrupt6(void);
void pulseCounterInterrupt7(void);
void pulseCounterInterrupt8(void);
*/
