//GET ppm from server
//GET latest counter values
//Read the last values from server and send to mega for display
//check log file if present when connection is active and send from it
//send ssid to mega for debugging
//problems when connecting to new network when previous network is not avilable

#include <FS.h>                   //this needs to be first, or it all crashes and burns...
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

// select wich pin will trigger the configuraton portal when set to LOW
// ESP-01 users please note: the only pins available (0 and 2), are shared
// with the bootloader, so always set them HIGH at power-up
#define TRIGGER_PIN 0

HardwareSerial & mega  = Serial;
HardwareSerial & debug = Serial1;

bool configDone = false;
bool dateSent = false;

char DeviceID[15];
char apiKey[15];
char date_string[32];

char ppm1[10];
char ppm2[10];
char ppm3[10];
char ppm4[10];
char ppm5[10];
char ppm6[10];
char ppm7[10];
char ppm8[10];

// The extra parameters to be configured (can be either global or just in the setup)
// After connecting, parameter.getValue() will get you the configured value
// id/name placeholder/prompt default length

WiFiManagerParameter custom_DeviceID("ID", "deviceID", DeviceID, 15);
WiFiManagerParameter custom_apiKey("Key", "apikey", apiKey, 15);
//Pulse per meter
WiFiManagerParameter custom_ppm1("ppm1", "ppm1", ppm1, 10);
WiFiManagerParameter custom_ppm2("ppm2", "ppm2", ppm2, 10);
WiFiManagerParameter custom_ppm3("ppm3", "ppm3", ppm3, 10);
WiFiManagerParameter custom_ppm4("ppm4", "ppm4", ppm4, 10);
WiFiManagerParameter custom_ppm5("ppm5", "ppm5", ppm5, 10);
WiFiManagerParameter custom_ppm6("ppm6", "ppm6", ppm6, 10);
WiFiManagerParameter custom_ppm7("ppm7", "ppm7", ppm7, 10);
WiFiManagerParameter custom_ppm8("ppm8", "ppm8", ppm8, 10);

//int entries = 0;
int position = 0; //position in log file
String parameters = ""; //device parameters
String jsondata = "";
String request;

String Key = "";
String DevID = "";

char response[500]; //from server and from mega

const char* url = "productionmeter.in";
const char* host = "productionmeter.in:8081";
const int httpPort = 8081;
// const char* url = "black-electronics.com";
// const char* host = "black-electronics.com";
// const int httpPort = 80;

bool DataReceived = false;
bool begin = false;
bool printParams= false;
bool logFileAvailable = false;
bool serverConnection = false;

WiFiClient client; //for creating tcp connections

/////Function declarations
void readClient(void);
void writeRequest(WiFiClient& client, String jsondata);
String prepareJSON(String jsondata);
void sendfromLOG(void);
void savetoFile(void);
void config(void);
void readLog(void);
void clearLog(void);
void readConfig(void); //tmp

void setup() {
  mega.begin(115200);
  debug.begin(115200);
  debug.setDebugOutput(true);
  pinMode(TRIGGER_PIN, INPUT);
  debug.println(F("\nmounting FS..."));
  if (SPIFFS.begin()) {
    debug.println(F("mounted FS"));
    if (SPIFFS.exists("/config.txt")) {
      debug.println(F("read config"));
      File configFile = SPIFFS.open("/config.txt", "r");
      if (configFile) {
        debug.println(F("config open"));
        parameters =  configFile.readStringUntil('\n');
        char tmp[300];
        parameters.toCharArray(tmp, 300);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(tmp);
        DevID = json["DeviceID"].asString();
        Key   = json["apikey"].asString();
        configFile.close();
      }
    }
    if (SPIFFS.exists("/log.txt")) {
      debug.println(F("Read log"));
      File logFile = SPIFFS.open("/log.txt", "r");
      if (logFile) {
        while(logFile.available()) {
          String entry =  logFile.readStringUntil('\n');
          if(entry.length() > 5) logFileAvailable = true;
          debug.println(entry);
        }
        logFile.close();
      }
    }
  }
  else {
    debug.println(F("FS mnt fail"));
  }
  //if config file read, send params to mega
  debug.println(parameters);
  memset(response, '\0', 500);    // Initialize the string
  //Begin Wifi in station mode
  if (WiFi.SSID()==""){
    //connect to Black electronics
    debug.println(F("We haven't got any access point credentials, so get them now"));
  }
  else{
    debug.println(F("Starting in Station Mode"));
    WiFi.mode(WIFI_STA); // Force to station mode because if device was switched off while in access point mode it will start up next time in access point mode.
    debug.println(F("Connected to:"));
    debug.print(WiFi.SSID());
  }
debug.println(F("loop.."));
}

void loop() {
  if (digitalRead(TRIGGER_PIN) == LOW  && !configDone) {
    config();
    //  save config here
    File config_file = SPIFFS.open("/config.txt", "w");
    if (!config_file) {
      debug.println("failed to open config file for writing");
    }
    config_file.println(parameters);
    config_file.close();
  }
  unsigned int x = 0;
  if (mega.available()>0){
    while(mega.available()){
      //cmds # and {..data..}
      response[x] = mega.read();
      debug.write(response[x]);
      //tmp
      if(response[x] == '$'){
        readConfig();
      }
      if(response[x] == '!'){
        readLog();
      }
      if(response[x] == '@'){
        clearLog();
      }
      if (response[x] == '#') //send params from esp
      {
          printParams = true;
      }
      if (response[x] == '{') {
       begin = true;
      }
      if (begin) jsondata += (response[x]);
      if (response[x] == '}') {
       begin =false;
       DataReceived = true;
      }
      x++;
    }
  }
  if(printParams){
    printParams = false;
    mega.println(parameters);
    mega.print("{\"SSID\":\"");mega.print(WiFi.SSID());mega.println("\"}");
    debug.println(parameters);
    debug.print("{\"SSID\":\"");debug.print(WiFi.SSID());debug.println("\"}");
  }
  if(DataReceived){
    DataReceived = false;
    if (!client.connect(url, httpPort)) {
      debug.println(F("conn failed"));
      mega.println("no ip");
      savetoFile(); //uncomment this
      serverConnection = false;
      jsondata = "";
      return;
    }
    serverConnection = true;
    request = prepareJSON(jsondata);
    writeRequest(client, request);
    readClient();

    jsondata = "";
    client.stop();
    memset(response, '\0', 500); // Initialize the string
  }
  //if net not down and file has data in it is present and has enough time elapsed since we were last time in this condition
  if(serverConnection && logFileAvailable){
    debug.println("Sending from backup");
    File logFile = SPIFFS.open("/log.txt","r");
    size_t size = logFile.size();
    if(size > position){ //test this
      logFile.seek(position,SeekSet);//seek to pos from begining of file
    }
    String entry =  logFile.readStringUntil('\n');//read line
    position = logFile.position(); //store position
    debug.println(entry);
    logFile.close();
    if(position == size){//reached end of file
      logFileAvailable = false;
      position = 0;
      clearLog();
    }
    else{
      if (!client.connect(url, httpPort)) {
        debug.println(F("conn failed"));
        mega.println("no ip");
        savetoFile();
        serverConnection = false;
        jsondata = "";
        return;
      }
      serverConnection = true;
      request = prepareJSON(entry);
      writeRequest(client, request);
      readClient();
      client.stop();
    }
  }
}//loop end
// Get:http://blackelectronic.cloudapp.net:8081/api/service/GetData?APIKey=6359147591&DeviceCode=111

//functions
void readClient(void){
  while(client.available()){
    String line;
    line.reserve(150);
    line = client.readStringUntil('\n');
    debug.println(line);
    char cline[150];
    line.toCharArray(cline, 150);// convert to char string from String
    if (strstr(cline, "201 Created")){
      mega.println("SEND OK");
    }
    if (strstr(cline, "Date:")){
      mega.println(cline);
    }
    // else {
    //   mega.println("no ip");
    // }
  }
}

void savetoFile(void){
  //enable a flag here, whcih will be read by main loop
  logFileAvailable = true;
  File logFile = SPIFFS.open("/log.txt", "a"); //open for write, stream at eof
  if(logFile.size() < 512000  && logFile){ //file size less than 500K\B
    logFile.println(jsondata);
    logFile.close();
    debug.println(F("Saved to File"));
  }
  else{
    debug.print(F("mem full"));
  }
}

void writeRequest(WiFiClient& client, String jsondata) {
  String PostRequest = "POST /api/service/Post HTTP/1.1\r\n";
  PostRequest       += "Host: productionmeter.in:8081\r\n";
  PostRequest       += "Content-Type: application/json\r\n";
  PostRequest       += "Content-Length: ";
  PostRequest       += jsondata.length();
  PostRequest       +="\r\n\r\n" + jsondata ;

  debug.println("\nRequesting URL: ");
  debug.println(PostRequest);
  client.println(PostRequest);
  jsondata = "";
}

String prepareJSON(String jsondata) {
  const int DecodeBUFFER_SIZE =
    JSON_OBJECT_SIZE(3) //DT,meters,BoxOpen
   +JSON_OBJECT_SIZE(8); //8 elements of meters

  StaticJsonBuffer<DecodeBUFFER_SIZE> jsonBuf; //for decoding counters from mega
  String json = "";
  json.reserve(300);
  char counters[300];

  jsondata.toCharArray(counters, 300);// convert to char string from String
  //decoding
  JsonObject& countTime = jsonBuf.parseObject(counters);

  const char* dateTime = countTime["DT"];
  int boxopen          = countTime["BoxOpen"];
  float c0             = countTime["Meters"][0];
  float c1             = countTime["Meters"][1];
  float c2             = countTime["Meters"][2];
  float c3             = countTime["Meters"][3];
  float c4             = countTime["Meters"][4];
  float c5             = countTime["Meters"][5];
  float c6             = countTime["Meters"][6];
  float c7             = countTime["Meters"][7];

  //encoding
  json = "{\"DT\":\"";
  json += dateTime;
  json += "\",\"DeviceId\":\"";
  json += DevID;
  json += "\",\"Meters\":[";
  json += String(c0,2);
  json += ",";
  json += String(c1,2);
  json += ",";
  json += String(c2,2);
  json += ",";
  json += String(c3,2);
  json += ",";
  json += String(c4,2);
  json += ",";
  json += String(c5,2);
  json += ",";
  json += String(c6,2);
  json += ",";
  json += String(c7,2);
  json += "],\"BoxOpen\":";
  json += boxopen;
  json += ",\"APIKey\":\"";
  json += Key;
  json += "\"}";

  jsondata = "";
  return json;
}

void config(){
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;

  wifiManager.addParameter(&custom_DeviceID);
  wifiManager.addParameter(&custom_apiKey);

  wifiManager.addParameter(&custom_ppm1);
  wifiManager.addParameter(&custom_ppm2);
  wifiManager.addParameter(&custom_ppm3);
  wifiManager.addParameter(&custom_ppm4);
  wifiManager.addParameter(&custom_ppm5);
  wifiManager.addParameter(&custom_ppm6);
  wifiManager.addParameter(&custom_ppm7);
  wifiManager.addParameter(&custom_ppm8);

  wifiManager.setTimeout(180);//config portal will be open for 3 mins

  if (!wifiManager.startConfigPortal("Black-Electronics","blackcorp")) {
    mega.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }
  strcpy(DeviceID, custom_DeviceID.getValue());
  strcpy(apiKey, custom_apiKey.getValue());

  strcpy(ppm1, custom_ppm1.getValue());
  strcpy(ppm2, custom_ppm2.getValue());
  strcpy(ppm3, custom_ppm3.getValue());
  strcpy(ppm4, custom_ppm4.getValue());
  strcpy(ppm5, custom_ppm5.getValue());
  strcpy(ppm6, custom_ppm6.getValue());
  strcpy(ppm7, custom_ppm7.getValue());
  strcpy(ppm8, custom_ppm8.getValue());

  debug.println(F("saving config"));
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& Params = jsonBuffer.createObject();
  Params["DeviceID"] = DeviceID;
  Params["apikey"] = apiKey;
  Params["ppm1"] = ppm1;
  Params["ppm2"] = ppm2;
  Params["ppm3"] = ppm3;
  Params["ppm4"] = ppm4;
  Params["ppm5"] = ppm5;
  Params["ppm6"] = ppm6;
  Params["ppm7"] = ppm7;
  Params["ppm8"] = ppm8;

  parameters = "";
  Params.printTo(parameters);
  Params.printTo(debug);
  //end save
  configDone = true;
  //if you get here you have connected to the WiFi
  debug.println("connected to client Wifi");
}

void readLog(void){
  if (SPIFFS.exists("/log.txt")) {
    debug.println("Reading log file");
    File logFile = SPIFFS.open("/log.txt", "r");
    while(logFile.available()) {
      String entry =  logFile.readStringUntil('\n');
      debug.println(entry);
    }
    logFile.close();
  }
  else{
    debug.println(F("No Log"));
  }
}

void readConfig(void){
  if (SPIFFS.exists("/config.txt")) {
    debug.println("Reading config file");
    File configFile = SPIFFS.open("/config.txt", "r");
    while(configFile.available()) {
      String entry =  configFile.readStringUntil('\n');
      debug.println(entry);
    }
    configFile.close();
  }
}

void clearLog(void){
  if (SPIFFS.exists("/log.txt")) {
    debug.println("Deleting log file");
    File logFile = SPIFFS.open("/log.txt", "w");
    logFile.close();
    debug.println("\nLog file cleaned");
  }
  else{
    debug.println(F("No Log"));
  }
}

/*
String PostRequest = "POST /api/service/Post HTTP/1.1\r\n";
PostRequest       += "Host: blackelectronic.cloudapp.net:8081\r\n";
PostRequest       += "Content-Type: application/json\r\n";
PostRequest       += "Content-Length: ";
PostRequest       += jsondata.length();
PostRequest       +="\r\n\r\n" + jsondata ; //change this to json

String PostRequest = "POST /sandbox/jsonpost.php HTTP/1.1\r\n";
PostRequest       += "Host: black-electronics.com\r\n";
PostRequest       += "Content-Type: application/json\r\n";
PostRequest       += "Content-Length: ";
PostRequest       += jsondata.length();
PostRequest       +="\r\n\r\n" + jsondata ; //change this to json
*/
