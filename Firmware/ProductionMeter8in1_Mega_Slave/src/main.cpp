//Production Meter8IN1 Mega slave
//Date: 25-Mar-16

//Show on LCD config read from eeprom or esp
//show device id
//esp will send the last updated values from server and update the variables and
//lcd display and count from there
//if config not done attempt every time data is ready

#include <Arduino.h>
#include <avr/wdt.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <Time.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <EnableInterrupt.h>
#include <DS1307RTC.h>

int connStatus=0;
//#define CycleButton 2 //use this to send $ so log file can be displayed on esp debug
bool netOK = false;     //checks internet is alive

const unsigned int DataupdateFreq = 30000; //update every 30s
const unsigned long DataupdateFreq_SD = 600000; //update every 10 minutes on SD(144 entries per 24 hrs)
unsigned long timeToWait;

const unsigned int lcdCycleDelay = 10000;
unsigned long previousMillis = 0; //for lcd cycle
int lcdState = 2; //for cycling display

bool debugging = false; //if password is ok, this is true
String debugPass = String("amethyst");
String password; //for storing input password

//Use GMT time(2:30AM -> 8:00AM)
const int ShiftStartHour = 2; //zero at 7:00AM
//const int ShiftStartHour = 3;//zeros at 9:00AM
const int ShiftStartMinute = 35 ; //30+5
//14:30 ->20:00(8:00PM)
const int ShiftEndHour = 14;
const int ShiftEndMinute = 35;  //35
// const int ShiftEndHour = 6;
// const int ShiftEndMinute = 5;  //35

int shiftNow=0;
int preShift=0;
int eepromShift;
const int eepromShiftAdd = 200;

//Date Time
tmElements_t tmx;
bool RTCUpdatedFlag= false; //true updated
int Restarted = 1;  //RTC update on each reboot
int getTimefromNW = 0;
unsigned long sentTime; //use for various waiting while loops

//For RTC update, updates every month from nw
int NewMonth;
int Month;

//For eeprom save each hour
int NewHour, Hour;

const int openDetect = 3;
bool BoxOpen = false;

const int RS = 12;
const int E  = 11;
const int D4 = 10;
const int D5 = 9;
const int D6 = 8;
const int D7 = 7;

LiquidCrystal lcd(RS,E,D4,D5,D6,D7);

struct Proxy{
  const int ProxyPin;
  const int counterEEpromAddress;
  const int pulsePerMeterEEpromAddress;
  float pulsePerMeter;
  volatile unsigned long PulseCounterVolatile;
  volatile float Meter;
  float preMeters;
  unsigned long PulseCounter;
  unsigned long NOW;
};
//pin,counterAddress,ppmaddress
Proxy P[8] = {{A8,  0,  8},
              {A9, 16, 24},
              {A10,32, 40},
              {A11,48, 56},
              {A12,64, 72},
              {A13,80, 88},
              {A14,96, 104},
              {A15,112, 120}
            };

HardwareSerial & debug    = Serial;
HardwareSerial & esp8266  = Serial1;

bool configDone = false;
int attempts = 0;
char json[350];
char cmdBuffer[100];
unsigned long debounceInterval = 2000;
String SSID;
const int showSSID = 2;

//function declaration
void printParams(void);
int8_t sendATcommand(const char* ATcommand,const char* expected_answer, unsigned int Timeout, boolean isDebug);
void sendData(int command);
void GetConfiguration(void);
String createJSON(void);
tmElements_t * string_to_tm(tmElements_t *tme, char *str);
void getConfigParams(String jsonConfig);
unsigned long EEPROM_Read_ULong(int address);
void EEPROM_Write_ULong(int address, unsigned long data);
void SaveCountersEEPROM(void);
void ReloadCountersEEPROM(void);
void updateLCD(void);
void cycleValues(void);
void pulseCounterInterrupt0(void);
void pulseCounterInterrupt1(void);
void pulseCounterInterrupt2(void);
void pulseCounterInterrupt3(void);
void pulseCounterInterrupt4(void);
void pulseCounterInterrupt5(void);
void pulseCounterInterrupt6(void);
void pulseCounterInterrupt7(void);
void ResetCounters(void);

void setup() {
  for (int i = 0; i < 8; i++) {
    P[i].NOW = 0;
  }
  esp8266.begin(115200);
  debug.begin(115200);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("BlackElectronics");
  lcd.setCursor(0,1);
  lcd.print("WiFi Prod. Meter");
  ///////////////////////////
  P[0].pulsePerMeter = 1;
  P[0].PulseCounterVolatile = 0;
  P[0].Meter = 0;
  P[0].preMeters = 0;
  P[0].PulseCounter = 0;

  P[1].pulsePerMeter = 1;
  P[1].PulseCounterVolatile = 0;
  P[1].Meter = 0;
  P[1].preMeters = 0;
  P[1].PulseCounter = 0;

  P[2].pulsePerMeter = 1;
  P[2].PulseCounterVolatile = 0;
  P[2].Meter = 0;
  P[2].preMeters = 0;
  P[2].PulseCounter = 0;

  P[3].pulsePerMeter = 1;
  P[3].PulseCounterVolatile = 0;
  P[3].Meter = 0;
  P[3].preMeters = 0;
  P[3].PulseCounter = 0;

  P[4].pulsePerMeter = 1;
  P[4].PulseCounterVolatile = 0;
  P[4].Meter = 0;
  P[4].preMeters = 0;
  P[4].PulseCounter = 0;

  P[5].pulsePerMeter = 1;
  P[5].PulseCounterVolatile = 0;
  P[5].Meter = 0;
  P[5].preMeters = 0;
  P[5].PulseCounter = 0;

  P[6].pulsePerMeter = 1;
  P[6].PulseCounterVolatile = 0;
  P[6].Meter = 0;
  P[6].preMeters = 0;
  P[6].PulseCounter = 0;

  P[7].pulsePerMeter = 1;
  P[7].PulseCounterVolatile = 0;
  P[7].Meter = 0;
  P[7].preMeters = 0;
  P[7].PulseCounter = 0;

  pinMode(openDetect,INPUT);
  pinMode(showSSID,INPUT);

  debug.println(F("---------------------"));
  debug.println(F("Black Electronics"));
  debug.println(F("WiFi Production Meter"));
  debug.println(F("---------------------"));
  debug.println(F("Please Enter Password:"));
  delay(3000);
  //Read password from user
  if(debug.available()>0){
    while(debug.available()){
      password = Serial.readStringUntil('\n');
    }
  }
  password.trim();//removes \n
  if(password == debugPass){ //
    debugging = true;
    debug.println(F("ok"));
  }
  else{
    debugging = true;
  }

  ReloadCountersEEPROM();
  for( int x = 0; x < 8; x++ ){
    pinMode(P[x].ProxyPin,INPUT_PULLUP);
  }

  enableInterrupt(P[0].ProxyPin, pulseCounterInterrupt0, FALLING);
  enableInterrupt(P[1].ProxyPin, pulseCounterInterrupt1, FALLING);
  enableInterrupt(P[2].ProxyPin, pulseCounterInterrupt2, FALLING);
  enableInterrupt(P[3].ProxyPin, pulseCounterInterrupt3, FALLING);
  enableInterrupt(P[4].ProxyPin, pulseCounterInterrupt4, FALLING);
  enableInterrupt(P[5].ProxyPin, pulseCounterInterrupt5, FALLING);
  enableInterrupt(P[6].ProxyPin, pulseCounterInterrupt6, FALLING);
  enableInterrupt(P[7].ProxyPin, pulseCounterInterrupt7, FALLING);

  if(RTC.read(tmx)){
    debug.println("RTC read");
  }
  else{
    if (RTC.chipPresent()) {
      debug.println(F("The RTC is stopped."));
    } else {
      debug.println(F("RTC read error!"));
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("RTC Error");
      delay(1000);
    }
  }
  NewMonth = Month = tmx.Month;
  NewHour = Hour = tmx.Hour;
  //Determine which shift we are in
  //if(tmx.Hour <= ShiftHour && tmx.Minute <ShiftMinute){
    if(ShiftStartHour <= tmx.Hour && tmx.Hour < ShiftEndHour){
    shiftNow = 1;
    preShift = 1;
  }
  else{
    shiftNow = 2;
    preShift = 2;
  }
  //read eeprom for shift data
  eepromShift = EEPROM.read(eepromShiftAdd);
  if(eepromShift != preShift){
    preShift = eepromShift;
    if(debugging) debug.println(F("---Shift changed"));
    //Clear counters
    ResetCounters();
  }
  if(debugging) debug.print(F("Shift: "));debug.println(shiftNow);
  //Shift times
  if(debugging) {
    //sprintf(cmdBuffer,"Shift Time: %d:%d(GMT)",ShiftStartH`our,ShiftStartMinute);
    //debug.println(cmdBuffer);
    //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[0])));
    //debug.println(cmdBuffer);
  }
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Reading Config..");
  GetConfiguration();
  debug.print(F("ConfigDoneInSetup:"));debug.println(configDone);
  lcd.clear();
  updateLCD();
}//Setup end

////////////LOOP START
void loop() {
  if(debug.available()>0){
    while (debug.available()) {
      char cmd = debug.read();
      debug.write(cmd);
      //tmp
      if(cmd == '!'){
        for (int i = 0; i < 8; i++) { //store in eeprom if params are different
          //int temp = EEPROM_Read_ULong(P[i].pulsePerMeterEEpromAddress);
          float temp;
          EEPROM.get(P[i].pulsePerMeterEEpromAddress,temp);
          debug.println(temp);
        }
      }
    }
  }
  //Determine the shift
  if(ShiftStartHour <= tmx.Hour  && tmx.Hour < ShiftEndHour){
    if(shiftNow != 1 && tmx.Minute ==30){
      shiftNow = 1;
      EEPROM.write(eepromShiftAdd,shiftNow);
      debug.println(F("Shift written on eeprom"));
    }
  }
  else{
    if(shiftNow != 2 && tmx.Minute ==30){
      shiftNow = 2;
      EEPROM.write(eepromShiftAdd,shiftNow);
      debug.println("Shift written on eeprom");
    }
  }
  //if shift has changed reset all counters to zero
  if(shiftNow != preShift){
    debug.println(F("---Shift changed"));
    preShift = shiftNow;
    noInterrupts();
    // Reset counter to zero at shift start
    ResetCounters();
    interrupts();
  }
  //Save counter every Hour
  NewHour = tmx.Hour;
  if(NewHour != Hour){
    Hour = NewHour;
    debug.println(); //New line for eeprom msg
    SaveCountersEEPROM();
  }
  //update rtc every month and every reboot
  NewMonth = tmx.Month;
  if( (NewMonth != Month && netOK)  || (Restarted && netOK)){
    Month = NewMonth;
    Restarted = 0;
    getTimefromNW =1;
    RTCUpdatedFlag = false;
    if(debugging) debug.println(F("\r\nRefreshing RTC..."));
  }
//-----------------------------------------------------------------------------
//Core
  if(configDone){
    //esp going in OnDemand AP mode
    //esp offline, read when sending data no ip
    if (getTimefromNW == 1) {
      sendData(1);
    }
    else{
       sendData(0);
     }
    if (connStatus == 3 ) {
      netOK = false;
      timeToWait = DataupdateFreq_SD;
      debug.println(F("Lowering freq next request after 10 mins"));
    }
    else if(connStatus == 1){
      timeToWait = DataupdateFreq;
      netOK = true;
    }
    //wait longer in loop
    sentTime = millis();
    debug.println(F("Updating lcd"));
    while(millis()-sentTime < timeToWait){
     updateLCD();
     if(tmx.Year <=40 || tmx.Year >= 70){ //Time glitch work around
       Restarted = 1; //update again
     }
     BoxOpen = !(digitalRead(openDetect)); //0 for closed, 1 open
    }
  }
  else{
    GetConfiguration();
    attempts++;
    debug.print(F("Attempts:"));debug.println(attempts);
    if(attempts >= 3){
      for (int i = 0; i < 8; i++) { //store in eeprom if params are different
        //float temp = EEPROM_Read_ULong(P[i].pulsePerMeterEEpromAddress);
        float temp;
        EEPROM.get(P[i].pulsePerMeterEEpromAddress,temp);
        P[i].pulsePerMeter = temp;
      }
      configDone = true;
      printParams();
    }
  }
}//loop end

//////functions
void printParams(void){
  debug.println(P[0].pulsePerMeter);
  debug.println(P[1].pulsePerMeter);
  debug.println(P[2].pulsePerMeter);
  debug.println(P[3].pulsePerMeter);
  debug.println(P[4].pulsePerMeter);
  debug.println(P[5].pulsePerMeter);
  debug.println(P[6].pulsePerMeter);
  debug.println(P[7].pulsePerMeter);
  debug.println(SSID);
}

void GetConfiguration(void){
  String str;
  unsigned long timeout = millis();
  while(!configDone && millis()- timeout < 5000){ //check if params have changed
    debug.println(F("Waiting for config"));
    esp8266.println("#");
    unsigned long sentTime = millis();
    while(millis() - sentTime < 1000){
      if(esp8266.available()){
        while(esp8266.available()){
          str = esp8266.readStringUntil('\n');
          if(debugging)debug.println(str);
          getConfigParams(str);
        }
      }
    }
  }
}

void getConfigParams(String jsonConfig){
    if (jsonConfig.length() > 3 ){
      jsonConfig.toCharArray(json, 350);// convert to char string from String
      const int BUFFER_SIZE = JSON_OBJECT_SIZE(18);// change this
      StaticJsonBuffer <BUFFER_SIZE> jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(json);
      if (!root.success()){
        debug.println(F("parseObject() failed"));
        configDone = false;
        return;
      }
      //{"ppm1":"2","ppm2":"1","ppm3":"1","ppm4":"1","ppm5":"1","ppm6":"1","ppm7":"1","ppm8":"1"}
      //{"ppm1":"665536.12","ppm2":"665536.12","ppm3":"665536.12","ppm4":"665536.12","ppm5":"665536.12","ppm6":"665536.12","ppm7":"665536.12","ppm8":"665536.12"}
      if(root["ppm1"] > 0) P[0].pulsePerMeter      = root["ppm1"];
      if(root["ppm2"] > 0) P[1].pulsePerMeter      = root["ppm2"];
      if(root["ppm3"] > 0) P[2].pulsePerMeter      = root["ppm3"];
      if(root["ppm4"] > 0) P[3].pulsePerMeter      = root["ppm4"];
      if(root["ppm5"] > 0) P[4].pulsePerMeter      = root["ppm5"];
      if(root["ppm6"] > 0) P[5].pulsePerMeter      = root["ppm6"];
      if(root["ppm7"] > 0) P[6].pulsePerMeter      = root["ppm7"];
      if(root["ppm8"] > 0) P[7].pulsePerMeter      = root["ppm8"];
      const char* ssid        = root["SSID"];
      SSID = ssid;
      for (int i = 0; i < 8; i++) { //store in eeprom if params are different
        //int temp = EEPROM_Read_ULong(P[i].pulsePerMeterEEpromAddress);
        float temp;
        EEPROM.get(P[i].pulsePerMeterEEpromAddress,temp);
        if (temp != P[i].pulsePerMeter) {
          //EEPROM_Write_ULong(P[i].pulsePerMeterEEpromAddress,P[i].pulsePerMeter);
          EEPROM.put(P[i].pulsePerMeterEEpromAddress,P[i].pulsePerMeter);
        }
      }
      printParams();
      configDone = true;
  }
  else{
    configDone = false;
  }
}

void updateLCD(void){
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= lcdCycleDelay){
    previousMillis = currentMillis;
    if (lcdState == 0 || lcdState == 2){
      //lcdState = 1; //cycle display
      lcdState = 0; // no cycle display
      lcd.clear();
    }
    else if (lcdState == 1){
      lcdState = 0;
      lcd.clear();
    }
  }
  if(lcdState == 0){
    lcd.setCursor(0,0);
    lcd.print("1:");lcd.print(P[0].Meter);lcd.print(",2:");lcd.print(P[1].Meter);
    lcd.setCursor(0,1);
    lcd.print("3:");lcd.print(P[2].Meter);lcd.print(",4:");lcd.print(P[3].Meter);
  }
  else if (lcdState == 1){
    lcd.setCursor(0,0);
    lcd.print("5:");lcd.print(P[4].Meter);lcd.print(",6:");lcd.print(P[5].Meter);
    lcd.setCursor(0,1);
    lcd.print("7:");lcd.print(P[6].Meter);lcd.print(",8:");lcd.print(P[7].Meter);
  }
  else if (lcdState == 2){
    lcd.setCursor(0,0);
    lcd.print(SSID);
  }
}

int8_t sendATcommand(const char* ATcommand,const char* expected_answer, unsigned int Timeout, boolean isDebug){
    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command
    }
    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
      if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
          response[x] = esp8266.read();
          if(isDebug && debugging){
              debug.print(response[x]);
          }
          x++;
          if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 1;
          }
          else if(strstr(response, "DNS Fail") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 2;
              break;
          }
          else if(strstr(response, "no ip") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 3;
              break;
          }
          else if(strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 0;
              break;
          }
      }
    updateLCD();
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out
  return answer;
}

void sendData(int command){
  //command = 0 -> Normal send
  //command = 1 -> update RTC
  //command = 2 -> send from SD card
  char date_string[32];
  String json;
  const char sendOK[] = "SEND OK";
  const char space[] = " ";

  json = createJSON();
  //debug.println(data);
  esp8266.print(json);
  if(debugging){
    debug.println(json);
  }
  //Parse the header for date
  if(command == 1 && !(RTCUpdatedFlag)){
    //update only if data is available from the network
     //Parse the returned header & web page. Looking for 'Date' line in header
    char dateString[] ="Date: ";
    if (esp8266.find(dateString))
    //if (esp8266.find("Date: "))
    {
      int i;
      for (i = 0; i < 31; i++) //31 this should capture the 'Date: ' line from the header
      {
        if (esp8266.available())  //new characters received?
        {
          char c = esp8266.read(); //print to console
          //debug.write(c);
          date_string[i] = c;
        }
        else i--;  //if not, keep going round loop until we've got all the characters
      }
    }
    string_to_tm(&tmx, date_string);
    //update RTC
    if(tmx.Year <= 70 && tmx.Year>=40){ //Time glitch work around
      if(RTC.write(tmx)){
        RTCUpdatedFlag = true;
        if(debugging) debug.println(F("\r\nRTC Updated"));
      }
    }
    if (RTC.read(tmx)) {
      if(debugging) {
        debug.print(F("Ok, Time = "));
        debug.print(tmx.Hour);
        debug.write(':');
        debug.print(tmx.Minute);
        debug.write(':');
        debug.print(tmx.Second);
        debug.print(F(", Date (D/M/Y) = "));
        debug.print(tmx.Day);
        debug.write('/');
        debug.print(tmx.Month);
        debug.write('/');
        debug.print(tmYearToCalendar(tmx.Year));
        debug.println();
      }
    }
  }
  connStatus = sendATcommand(space,sendOK,10000,debugging); //read this from esp
  debug.print(F("ConnStaus: "));debug.println(connStatus);
  //RTC update end


}//send data end

String createJSON(void){
  String json;
  json.reserve(300);
  RTC.read(tmx);
  json = "{";
  json += "\"DT\":\"";
  if(tmx.Day > 0 && tmx.Day <10){
    json += "0";
  }
  json += tmx.Day;
  json += "-";
  if(tmx.Month >0 && tmx.Month <10){
    json += "0";
  }
  json += tmx.Month;
  json += "-";
  json += tmYearToCalendar(tmx.Year);

  json += " ";
  if(tmx.Hour >=0 && tmx.Hour <10){
    json += "0";
  }
  json += tmx.Hour;
  json += ":";
  if(tmx.Minute >=0 && tmx.Minute <10){
    json += "0";
  }
  json += tmx.Minute;
  json += ":";
  if(tmx.Second >=0 && tmx.Second <10){
    json += "0";
  }
  json += tmx.Second;
  json += "\",\"Meters\":[";
  //no interrupes
  json += P[0].Meter;
  json += ",";
  json += P[1].Meter;
  json += ",";
  json += P[2].Meter;
  json += ",";
  json += P[3].Meter;
  json += ",";
  json += P[4].Meter;
  json += ",";
  json += P[5].Meter;
  json += ",";
  json += P[6].Meter;
  json += ",";
  json += P[7].Meter;
  json += "]";
  json += ",\"BoxOpen\":";
  json += BoxOpen;
  json += "}";

  return json;
}

tmElements_t * string_to_tm(tmElements_t *tme, char *str) {
  // Sat, 28 Mar 2015 13:53:38 GMT
  const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  char *r, *i, *t;
  r = strtok_r(str, " ", &i);

  r = strtok_r(NULL, " ", &i);
  tme->Day = atoi(r);

  r = strtok_r(NULL, " ", &i);
  for (int i = 0; i < 12; i++) {
    if (!strcmp(months[i], r)) {
      tme->Month = i + 1;
      break;
    }
  }
  r = strtok_r(NULL, " ", &i);
  tme->Year = atoi(r) - 1970;

  r = strtok_r(NULL, " ", &i);
  t = strtok_r(r, ":", &i);
  tme->Hour = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Minute = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Second = atoi(t);

  return tme;
}

unsigned long EEPROM_Read_ULong(int address){
  unsigned long temp = 0 ;
  for (byte i=0; i<8; i++)
    temp = (temp << 8) + EEPROM.read(address++);
  return temp;
}

void EEPROM_Write_ULong(int address, unsigned long data){
  for (byte i=0; i<8; i++)
  {
    EEPROM.write(address+7-i, data);
    data = data >> 8;
  }
}

void SaveCountersEEPROM(void){
  //read from eeprom, compare with current values
  //update counters whose value has changed
  EEPROM_Write_ULong(P[0].counterEEpromAddress,P[0].PulseCounter);
  EEPROM_Write_ULong(P[1].counterEEpromAddress,P[1].PulseCounter);
  EEPROM_Write_ULong(P[2].counterEEpromAddress,P[2].PulseCounter);
  EEPROM_Write_ULong(P[3].counterEEpromAddress,P[3].PulseCounter);
  EEPROM_Write_ULong(P[4].counterEEpromAddress,P[4].PulseCounter);
  EEPROM_Write_ULong(P[5].counterEEpromAddress,P[5].PulseCounter);
  EEPROM_Write_ULong(P[6].counterEEpromAddress,P[6].PulseCounter);
  EEPROM_Write_ULong(P[7].counterEEpromAddress,P[7].PulseCounter);
}
void ReloadCountersEEPROM(void){
  P[0].PulseCounter = EEPROM_Read_ULong(P[0].counterEEpromAddress);
  P[1].PulseCounter = EEPROM_Read_ULong(P[1].counterEEpromAddress);
  P[2].PulseCounter = EEPROM_Read_ULong(P[2].counterEEpromAddress);
  P[3].PulseCounter = EEPROM_Read_ULong(P[3].counterEEpromAddress);
  P[4].PulseCounter = EEPROM_Read_ULong(P[4].counterEEpromAddress);
  P[5].PulseCounter = EEPROM_Read_ULong(P[5].counterEEpromAddress);
  P[6].PulseCounter = EEPROM_Read_ULong(P[6].counterEEpromAddress);
  P[7].PulseCounter = EEPROM_Read_ULong(P[7].counterEEpromAddress);

  P[0].PulseCounterVolatile = P[0].PulseCounter;
  P[1].PulseCounterVolatile = P[1].PulseCounter;
  P[2].PulseCounterVolatile = P[2].PulseCounter;
  P[3].PulseCounterVolatile = P[3].PulseCounter;
  P[4].PulseCounterVolatile = P[4].PulseCounter;
  P[5].PulseCounterVolatile = P[5].PulseCounter;
  P[6].PulseCounterVolatile = P[6].PulseCounter;
  P[7].PulseCounterVolatile = P[7].PulseCounter;
  //Calulate Meters from saved Counter
  P[0].Meter = ((float)P[0].PulseCounter)/P[0].pulsePerMeter;
  P[1].Meter = ((float)P[1].PulseCounter)/P[1].pulsePerMeter;
  P[2].Meter = ((float)P[2].PulseCounter)/P[2].pulsePerMeter;
  P[3].Meter = ((float)P[3].PulseCounter)/P[3].pulsePerMeter;
  P[4].Meter = ((float)P[4].PulseCounter)/P[4].pulsePerMeter;
  P[5].Meter = ((float)P[5].PulseCounter)/P[5].pulsePerMeter;
  P[6].Meter = ((float)P[6].PulseCounter)/P[6].pulsePerMeter;
  P[7].Meter = ((float)P[7].PulseCounter)/P[7].pulsePerMeter;
}
/////////////////////////////
void pulseCounterInterrupt0(void){
  if(millis() - P[0].NOW > debounceInterval){
    P[0].PulseCounterVolatile++;
    P[0].NOW = millis();
  }
  P[0].PulseCounter = P[0].PulseCounterVolatile;
  P[0].Meter = ((float)P[0].PulseCounter)/P[0].pulsePerMeter;
}
void pulseCounterInterrupt1(void){
  if(millis() - P[1].NOW > debounceInterval){
    P[1].PulseCounterVolatile++;
    P[1].NOW = millis();
  }
  P[1].PulseCounter = P[1].PulseCounterVolatile;
  P[1].Meter = ((float)P[1].PulseCounter)/P[1].pulsePerMeter;
}
void pulseCounterInterrupt2(void){
  if(millis() - P[2].NOW > debounceInterval){
    P[2].PulseCounterVolatile++;
    P[2].NOW = millis();
  }
  P[2].PulseCounter = P[2].PulseCounterVolatile;
  P[2].Meter = ((float)P[2].PulseCounter)/P[2].pulsePerMeter;
}
void pulseCounterInterrupt3(void){
  if(millis() - P[3].NOW > debounceInterval){
    P[3].PulseCounterVolatile++;
    P[3].NOW = millis();
  }
  P[3].PulseCounter = P[3].PulseCounterVolatile;
  P[3].Meter = ((float)P[3].PulseCounter)/P[3].pulsePerMeter;
}
void pulseCounterInterrupt4(void){
  if(millis() - P[4].NOW > debounceInterval){
    P[4].PulseCounterVolatile++;
    P[0].NOW = millis();
  }
  P[4].PulseCounter = P[4].PulseCounterVolatile;
  P[4].Meter = ((float)P[4].PulseCounter)/P[4].pulsePerMeter;
}
void pulseCounterInterrupt5(void){
  if(millis() - P[5].NOW > debounceInterval){
    P[5].PulseCounterVolatile++;
    P[5].NOW = millis();
  }
  P[5].PulseCounter = P[5].PulseCounterVolatile;
  P[5].Meter = ((float)P[5].PulseCounter)/P[5].pulsePerMeter;
}
void pulseCounterInterrupt6(void){
  if(millis() - P[6].NOW > debounceInterval){
    P[6].PulseCounterVolatile++;
    P[6].NOW = millis();
  }
  P[6].PulseCounter = P[6].PulseCounterVolatile;
  P[6].Meter = ((float)P[6].PulseCounter)/P[6].pulsePerMeter;
}
void pulseCounterInterrupt7(void){
  if(millis() - P[7].NOW > debounceInterval){
    P[7].PulseCounterVolatile++;
    P[7].NOW = millis();
  }
  P[7].PulseCounter = P[7].PulseCounterVolatile;
  P[7].Meter = ((float)P[7].PulseCounter)/P[7].pulsePerMeter;
}

void ResetCounters(void){
  P[0].Meter = 0;
  P[0].PulseCounterVolatile = 0;
  P[0].PulseCounter = 0;

  P[1].Meter = 0;
  P[1].PulseCounterVolatile = 0;
  P[1].PulseCounter = 0;

  P[2].Meter = 0;
  P[2].PulseCounterVolatile = 0;
  P[2].PulseCounter = 0;

  P[3].Meter = 0;
  P[3].PulseCounterVolatile = 0;
  P[3].PulseCounter = 0;

  P[4].Meter = 0;
  P[4].PulseCounterVolatile = 0;
  P[4].PulseCounter = 0;

  P[5].Meter = 0;
  P[5].PulseCounterVolatile = 0;
  P[5].PulseCounter = 0;

  P[6].Meter = 0;
  P[6].PulseCounterVolatile = 0;
  P[6].PulseCounter = 0;

  P[7].Meter = 0;
  P[7].PulseCounterVolatile = 0;
  P[7].PulseCounter = 0;

  SaveCountersEEPROM();
}

/*
Eprom Memory map
Counter   Address   ppmAddress
1         0-7       8-15
2         16-23     24-31
3         32-39     40-47
4         48-55     56-63
5         64-71     72-79
6         80-87     88-95
7         96-103    104-111
8         112-119   120-127
PlusePerMeter
Shift Address 200

*/
