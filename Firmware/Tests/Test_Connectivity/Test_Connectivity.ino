//Testeed connectivity 
//Tested Json
//Testin Counter
//Production Meter
//Date:20-Jan-16
//arduino 1.0.6

//ESP flush

//TODO
//Find cause of crash and reboots found WDT
// other causes, not net on reboot loop in get time stuff
//cause reboots while reading response from esp


#include <avr/wdt.h>
#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <SD.h>        
#include <EEPROM.h>
#include <LiquidCrystal.h>

#define debugging true
///Counter Variables
#define PULSESPERMETER    45 //how many pulses from sensor equal 1 meter of cloth
#define CPMTHRESHOLD       8000  // GPM will reset after this many MS if no pulses are registered
#define INTERRUPTPIN   2  // INT4 PE4 pin 6 D2
#define LED            13 // Moteinos have LEDs on D9
#define XMITPERIOD         5000  // GPMthreshold should be less than 2*XMITPERIOD
#define NOT_AN_INTERRUPT -1 // silent the compiler on 1.0.6

volatile byte ledState = LOW;
volatile unsigned long PulseCounterVolatile = 0; // use volatile for shared variables

unsigned long NOW = 0;
unsigned long PulseCounter = 0;
unsigned long LASTMINUTEMARK = 0;
unsigned long PULSECOUNTLASTMINUTEMARK = 0; //keeps pulse count at the last minute mark

byte COUNTEREEPROMSLOTS = 10;
unsigned long COUNTERADDRBASE = 8; //address in EEPROM that points to the first possible slot for a counter
unsigned long COUNTERADDR = 0;     //address in EEPROM that points to the latest Counter in EEPROM
byte secondCounter = 0;

unsigned long TIMESTAMP_pulse_prev = 0;
unsigned long TIMESTAMP_pulse_curr = 0;
int pulseAVGInterval = 0;
int pulsesPerXMITperiod = 0;
float CPM=0, CLM=0, METERS=0, METERSlast=0, CPMlast=0, CLMlast=0;
//CPM:cloth per minute
//CLM:cloth last minute
bool ClothPresent = true;
bool BoxOpen = false;
int Temperature=0;
char buff[80];

char* METERSstr; //longest expected GAL message
char* CPMstr; //longest expected GPM message
char* CLMstr; //longest expected GLM message
///Counter variables end

String JSON; //temp delete it

Sd2Card card;
SdVolume volume;
SdFile root;
File Datalog;

//HardwareSerial & debug    = Serial2; //at installation
HardwareSerial & debug    = Serial;
HardwareSerial & esp8266  = Serial1;

//const int LED = 15;
//Store them in progmem
const char DeviceID[] = "Demo1"; 
const char APIKey[]  = "faiviasecrboefloresefgxtemn"; 
tmElements_t shift1; //For storing shiftTimes
//shift1.Hour = 7;
//shift1.Minute = 30;
tmElements_t shift2; //For storing shiftTimes
//shift2.Hour = 19;
//shift2.Minute = 30;

//AT commands stored in flash
const char at[] PROGMEM=            "AT";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char rst[] PROGMEM =          "AT+RST";
const char cwjap[] PROGMEM =        "AT+CWJAP=\"HemalChevliOffice\",\"blackcorp\"";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"krtyadev1.cloudapp.net\",8082";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";

//pointers to AT commands
const char*  AT PROGMEM = at;
const char*  CIPSTATUS PROGMEM = cipstatus;
const char*  RESET PROGMEM = rst;
const char*  CWJAP PROGMEM = cwjap;
const char*  CIPSTART PROGMEM = cipstart;
const char*  CIPCLOSE PROGMEM = cipclose;
const char*  CIFSR PROGMEM = cifsr;
const char*  CIPMUX PROGMEM = cipmux;
char cmdBuffer[100];
//{"DT":123}

//const char POST[] PROGMEM =           "POST /sandbox/jsonpost.php HTTP/1.1";//32 
//const char HOST[] PROGMEM =           "Host: black-electronics.com"; //16
const char POST[] PROGMEM =           "POST /api/service/InsertLog HTTP/1.1"; 
const char HOST[] PROGMEM =           "Host: krtyadev1.cloudapp.net:8082"; 
const char contentType[] PROGMEM =    "Content-Type: application/json"; //30
const char contentLength[] PROGMEM =  "Content-Length: "; //16 +10(LF of each line) + jsonlenlen(3)  +jsonlen(373)

const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};

int postlen = 0;
int connection;

//Date Time
tmElements_t tmx;
bool RTCUpdatedFlag= false; //true updated, 0 not updated
int Restarted =1;
int getTimefromNW = 0;

int fileSent = 0; //0 not sent 1 sent
int cipstartStatus;//main
//uint16_t entries; //check if 
//uint16_t sent_entries; //save in eeprom 

unsigned long entries; //check if 
unsigned long sent_entries; //save in eeprom 

long sentTime; //main
bool trasnfer_in_progress = false;
bool SDcardPresent = false;
const int CardDetect =49; //Use internal Pull up

//Edit these 
const int RS = 12;
const int E  = 11;
const int D4 = 10;
const int D5 = 9;
const int D6 = 8;
const int D7 = 7;

LiquidCrystal lcd(RS,E,D4,D5,D6,D7);

//Function Declaration
String createJSON(void);
void sendData(int updateRTC);
byte countDigits(int num);
tmElements_t * string_to_tm(tmElements_t *tme, char *str);
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug);
int8_t connStatus(void);
bool netConnected(void);
void save2SD(void);
void sendfromSD(void);
void EEPROMWritelong(int address, long value);
long EEPROMReadlong(long address);
unsigned long EEPROM_Read_Counter();
void EEPROM_Write_Counter(unsigned long counterNow);
unsigned long EEPROM_Read_ULong(int address);
void EEPROM_Write_ULong(int address, unsigned long data);
void XMIT(void);
void refreshCounter(void);
void readBoxTemp(void);

void setup(){

  lcd.begin(16, 2); 
  debug.begin(115200);
  esp8266.begin(115200);
  
  //SD setup
  pinMode(53, OUTPUT); //53 is chip select
  if (!SD.begin(53)) {
    debug.println(F("\r\nInitialization failed!"));
    SDcardPresent = false;
    return;
  }
  debug.println(F("\r\nInitialization done."));
  SDcardPresent = true;

  //espsetup
  postlen = sizeof(POST) + sizeof(HOST) + sizeof(contentType) + sizeof(contentLength) + 7; 
  sendATcommand("AT","OK",1000,debugging);
  delay(2000);//WAIT FOR MODULE TO CONNECT TO AP
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIFSR))); 
  sendATcommand(cmdBuffer,"OK",5000,debugging); //WILL PRINT 0.0.0.0 IF NOT CONNECTED

  pinMode(LED, OUTPUT);
  //read out contents of SD card
  if(SD.exists("LOG.TXT")){
    fileSent = 0;
    debug.println(F("Read size of file"));
    Datalog = SD.open("LOG.TXT");
    while(Datalog.available()){
      if (Datalog.read() == '\r'){ //start of new dataline
        entries++; //count jsons 
        }
    }
    debug.println(entries);
    entries = 0;
    Datalog.close();
  }
  //Check if box is open or read from eeprom
  //Counter setup
  //write in eeprom
  //write message on LCD
  //while(1);
  unsigned long savedCounter = EEPROM_Read_Counter();
  if (savedCounter <=0) savedCounter = 1; //avoid division by 0
  PulseCounterVolatile = PulseCounter = PULSECOUNTLASTMINUTEMARK = savedCounter;
  attachInterrupt(digitalPinToInterrupt(INTERRUPTPIN), pulseCounterInterrupt, RISING);
  //

  //Read sent entries from eeprom
  debug.println(F("\r\nGoing in Loop----------------------------------------------"));
  
  
/*
  String JSON = createJSON();
  debug.println(JSON);
  debug.println("-------------");
  debug.print("postlen: "); debug.println(postlen); 
  debug.println("-------------");
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART)));
  sendATcommand(cmdBuffer,"OK",5000,debugging);
  sendData(0);
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
  sendATcommand(cmdBuffer,"OK",2000,debugging);
  */
}

void loop(){
//get counter and print json
//JSON = createJSON();
  //wait 60 s
  //save data in eeprom

delay(2000);
}//loop end
//////////////////////////////////////////////////////////

///Functions

  //check eeprom for sent entries
  //copy to sententries variable in RAM
  //if present run for loop to jump to appropriate location to send
void readBoxTemp(void){
  //read io
  //read temp
}

void refreshCounter(void){
   noInterrupts();
  PulseCounter = PulseCounterVolatile;
  interrupts();
  METERS = ((float)PulseCounter)/PULSESPERMETER;
  CPM = pulseAVGInterval > 0 ? 60.0 * 1000 * (1.0/PULSESPERMETER)/(pulseAVGInterval/pulsesPerXMITperiod): 0;
  //if 1 hour  have passed save data in eeprom use RTC for time check
  //if curr hour != prehour
    //save data
}

byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
}

void sendData(int command){
  //command = 0 -> Normal send
  //command = 1 -> update RTC
  //command = 2 -> send from SD card
  char date_string[32];
  char *datep = &date_string[0];
  String json;
  int jsonLen;
  int jsonlenlen;
  int cipsendLen;

    json = createJSON();
    jsonLen = json.length();
    jsonlenlen = countDigits(jsonLen);
    cipsendLen = jsonLen + jsonlenlen + postlen ; //when all zeros cipsend is 480
    debug.println(json);
    debug.println(cipsendLen);
    sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    sendATcommand(cmdBuffer,">",1000,debugging);
    for (int i = 0; i <=2 ; i++){
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
      esp8266.println(cmdBuffer);
      delay(50);
    }
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
    esp8266.print(cmdBuffer);
    esp8266.println(jsonLen);
    esp8266.println();
    delay(50);
    esp8266.print(json);
    sendATcommand(" ","SEND OK",5000,debugging); //send compelete

  //Parse the header
  if(command == 1 && !(RTCUpdatedFlag)){
    //update only if data is available from the network
     //Parse the returned header & web page. Looking for 'Date' line in header
    if (esp8266.find("Date: ")) //get the date line from the http header (for example)
    {
      int i;
      for (i = 0; i < 31; i++) //31 this should capture the 'Date: ' line from the header
      {
        if (esp8266.available())  //new characters received?
        {
          char c = esp8266.read(); //print to console
          debug.write(c);
          date_string[i] = c;
        }
        else i--;  //if not, keep going round loop until we've got all the characters
      }
    }
    string_to_tm(&tmx, date_string);
    //update RTC
    if(RTC.write(tmx)){
      RTCUpdatedFlag = true;
      debug.println(F("RTC Updated"));
    }
    if (RTC.read(tmx)) {
      debug.print(F("Ok, Time = "));
      debug.print(tmx.Hour);
      debug.write(':');
      debug.print(tmx.Minute);
      debug.write(':');
      debug.print(tmx.Second);
      debug.print(F(", Date (D/M/Y) = "));
      debug.print(tmx.Day);
      debug.write('/');
      debug.print(tmx.Month);
      debug.write('/');
      debug.print(tmYearToCalendar(tmx.Year));
      debug.println();
    } 
  }//RTC update end  
}

int8_t connStatus(void){
    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTATUS))); 
    esp8266.println(cmdBuffer); //send cipstatus command
    x = 0;
    previous = millis();
    // this loop waits for the answer
    do{
        if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
            response[x] = esp8266.read();
            if(debugging){
                debug.print(response[x]);
            }
            x++;
            if      (strstr(response, "STATUS:2") != NULL)    
            {
                answer = 2;
            }
            else if (strstr(response, "STATUS:3") != NULL)  
            {
                answer = 3;
            }
            else if (strstr(response, "STATUS:4") != NULL)  
            {
                answer = 4;
            }
            else if (strstr(response, "STATUS:5") != NULL)  
            {
                answer = 5;
            }
        }
    }while((answer == 0) && ((millis() - previous) < 5000));    // Waits for the asnwer with time out

    return answer;
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command 
    }

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
    if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
        response[x] = esp8266.read();
        if(isDebug && debugging){
            debug.print(response[x]);
        }
        x++;
        if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 1;
        }
        else if(strstr(response, "DNS Fail") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 2;
            break;
        }
        else if(strstr(response, "no ip") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 3;
            break;
        }
        else if(strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 0;
            break;
        }
    }
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out
  return answer;
}

String createJSON(void){

  String json;
  //refreshCounter();
  //readBoxTemp();
  RTC.read(tmx);
  json = "{";
  json += "\"DT\":\"";
  if(tmx.Day >0 && tmx.Day <10){
    json += "0";
  }
  json += tmx.Day;
  json += "-";
  if(tmx.Month >0 && tmx.Month <10){
    json += "0";
  }
  json += tmx.Month;
  json += "-";
  json += tmYearToCalendar(tmx.Year);

  json += " ";
  if(tmx.Hour >=0 && tmx.Hour <10){
    json += "0";
  }
  json += tmx.Hour;
  json += ":";
  if(tmx.Minute >=0 && tmx.Minute <10){
    json += "0";
  }
  json += tmx.Minute;
  json += ":";
  if(tmx.Second >=0 && tmx.Second <10){
    json += "0";
  }
  json += tmx.Second;
  json +="\",\"DeviceID\":\"";
  json += DeviceID;
  json += "\",\"Meters\":";
  json += METERS;
  json += ",\"MeterPerMinute\":";
  json += CPM;
  json += ",\"ClothPresent\":";
  json += ClothPresent;
  json += ",\"Temperature\":";
  json += Temperature;
  json += ",\"BoxOpen\":";
  json += BoxOpen;
  json += ",\"APIKey\":\"";
  json += APIKey;
  json += "\"}";
//{"DT":"28-01-2016 18:38","DeviceID":1,"Meters":3123,"MeterPerMinute":32
//,"ClothPresent":1,"Temperature":40,"BoxOpen":0,"APIKey":"dafsdfasf4fadsf234"}
  //noInterrupts();
  //PulseCounter = PulseCounterVolatile;
  //interrupts();
  return json;
}


tmElements_t * string_to_tm(tmElements_t *tme, char *str) {
  // Sat, 28 Mar 2015 08:53:38 GMT

  const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

  char *r, *i, *t;
  r = strtok_r(str, " ", &i);

  r = strtok_r(NULL, " ", &i);
  tme->Day = atoi(r);

  r = strtok_r(NULL, " ", &i);
  for (int i = 0; i < 12; i++) {
    if (!strcmp(months[i], r)) {
      tme->Month = i + 1;
      break;
    }
  }

  r = strtok_r(NULL, " ", &i);
  tme->Year = atoi(r) - 1970;

  r = strtok_r(NULL, " ", &i);
  t = strtok_r(r, ":", &i);
  tme->Hour = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Minute = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Second = atoi(t);

  return tme;
}
//This function will write a 4 byte (32bit) long to the eeprom at
//the specified address to address + 3.
void EEPROMWritelong(int address, long value){
  //Decomposition from a long to 4 bytes by using bitshift.
  //One = Most significant -> Four = Least significant byte
  byte four = (value & 0xFF);
  byte three = ((value >> 8) & 0xFF);
  byte two = ((value >> 16) & 0xFF);
  byte one = ((value >> 24) & 0xFF);

  //Write the 4 bytes into the eeprom memory.
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);
}

long EEPROMReadlong(long address){
  //Read the 4 bytes from the eeprom memory.
  long four = EEPROM.read(address);
  long three = EEPROM.read(address + 1);
  long two = EEPROM.read(address + 2);
  long one = EEPROM.read(address + 3);

  //Return the recomposed long by using bitshift.
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}

unsigned long EEPROM_Read_Counter()
{
  return EEPROM_Read_ULong(EEPROM_Read_ULong(COUNTERADDR));
}

void EEPROM_Write_Counter(unsigned long counterNow)
{
  if (counterNow == EEPROM_Read_Counter())
  {
    debug.print("{EEPROM-SKIP(no changes)}");
    return; //skip if nothing changed
  }
  
  debug.print("{EEPROM-SAVE(");
  debug.print(EEPROM_Read_ULong(COUNTERADDR));
  debug.print(")=");
  debug.print(PulseCounter);
  debug.print("}");
    
  unsigned long CounterAddr = EEPROM_Read_ULong(COUNTERADDR);
  if (CounterAddr == COUNTERADDRBASE+8*(COUNTEREEPROMSLOTS-1))
    CounterAddr = COUNTERADDRBASE;
  else CounterAddr += 8;
  
  EEPROM_Write_ULong(CounterAddr, counterNow);
  EEPROM_Write_ULong(COUNTERADDR, CounterAddr);
}

unsigned long EEPROM_Read_ULong(int address){

  unsigned long temp;
  for (byte i=0; i<8; i++)
    temp = (temp << 8) + EEPROM.read(address++);
  return temp;
}

void EEPROM_Write_ULong(int address, unsigned long data)
{
  for (byte i=0; i<8; i++)
  {
    EEPROM.write(address+7-i, data);
    data = data >> 8;
  }
}

void pulseCounterInterrupt()
{
  noInterrupts();
  ledState = !ledState;
  PulseCounterVolatile++;  // increase when LED turns on
  digitalWrite(LED, ledState);
  NOW = millis();

  //remember how long between pulses (sliding window)
  TIMESTAMP_pulse_prev = TIMESTAMP_pulse_curr;
  TIMESTAMP_pulse_curr = NOW;
  
  if (TIMESTAMP_pulse_curr - TIMESTAMP_pulse_prev > CPMTHRESHOLD)
    //more than 'GPMthreshold' seconds passed since last pulse... resetting GPM
    pulsesPerXMITperiod=pulseAVGInterval=0;
  else
  {
    pulsesPerXMITperiod++;
    pulseAVGInterval += TIMESTAMP_pulse_curr - TIMESTAMP_pulse_prev;
  }
  interrupts();
}

void XMIT(void){
  debug.println("1");
  noInterrupts();
  PulseCounter = PulseCounterVolatile;
  interrupts();
 
  //calculate Gallons counter 
  METERS = ((float)PulseCounter)/PULSESPERMETER;
  Serial.print("PulseCounter:");Serial.print(PulseCounter);Serial.print(", Meter: "); Serial.print(METERS);

  //calculate & output GPM
  CPM = pulseAVGInterval > 0 ? 60.0 * 1000 * (1.0/PULSESPERMETER)/(pulseAVGInterval/pulsesPerXMITperiod): 0;
  dtostrf(METERS,3,2, METERSstr);
  dtostrf(CPM,3,2, CPMstr);

  pulsesPerXMITperiod = 0;
  pulseAVGInterval = 0;
  secondCounter += XMITPERIOD/1000;
  debug.println("4");
  //once per minute, output a GallonsLastMinute count
  if (secondCounter>=60)
  {
    //Serial.print("60sec mark ... ");
    secondCounter=0;
    CLM = ((float)(PulseCounter - PULSECOUNTLASTMINUTEMARK))/PULSESPERMETER;
    PULSECOUNTLASTMINUTEMARK = PulseCounter;
    EEPROM_Write_Counter(PulseCounter);
    dtostrf(CLM,3,2, CLMstr);
    sprintf(buff, "Meter:%s CPM:%s CLM:%s", METERSstr, CPMstr, CLMstr);
   
  }
  else
  {
    sprintf(buff, "METER:%s CPM:%s", METERSstr, CPMstr);    
  }
  debug.println("5");
  if (CPM!=CPMlast || METERS !=METERSlast || CLM!=CLMlast)
  {
    //sendLen = strlen(buff);
    //radio.sendWithRetry(GATEWAYID, buff, sendLen);
    //Send data 
    METERSlast = METERS;
    CPMlast = CPM;
    CLMlast = CLM;
  }
  debug.print(buff);
  debug.println("6");
}
