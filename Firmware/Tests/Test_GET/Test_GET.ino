//Production Meter
//Date:20-Jan-16
//arduino 1.0.6

//TODO
//Hangs with busy p... botch and do hard reset, use debug pins
//Get latest value from server
//shift flag in json
//data from SD flag
//skip writing to SD if meters is same.
//Remove SD save and send
//send data is updated
//save data on sd at shift end only if net is down
//display net down on lcd

/*
Things to update at installation
  FORMAT sd CARD
  Run SetRTC with full compailation
  Run clearSD and eeprom program
  SET AT+CWMODE=1
  join wifi network
  CIPStart post host
  DeviceID
  API key
  change Shift time
  Pulsespermeter
  CPM threshold time
  clothThreshold
  remove debugging true in setup 
*/
#include <avr/wdt.h>
#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <SD.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>

///Counter Variables
#define PULSESPERMETER    1 //how many pulses from sensor equal 1 meter of cloth(100/(Pi*Dcm)
#define CPMTHRESHOLD       5000  // GPM will reset after this many MS if no pulses are registered
#define INTERRUPTPIN   2  // INT4 PE4 pin 6 D2
#define LED            13 // Moteinos have LEDs on D9
#define NOT_AN_INTERRUPT -1 // silent the compiler on 1.0.6

volatile byte ledState = LOW;
volatile unsigned long PulseCounterVolatile = 0; // use volatile for shared variables

unsigned long NOW = 0;
unsigned long PulseCounter = 0;
unsigned long LASTMINUTEMARK = 0;
unsigned long PULSECOUNTLASTMINUTEMARK = 0; //keeps pulse count at the last minute mark

byte COUNTEREEPROMSLOTS = 10;
unsigned long COUNTERADDRBASE = 8; //address in EEPROM that points to the first possible slot for a counter
unsigned long COUNTERADDR = 0;     //address in EEPROM that points to the latest Counter in EEPROM
byte secondCounter = 0;

unsigned long TIMESTAMP_pulse_prev = 0;
unsigned long TIMESTAMP_pulse_curr = 0;
int pulseAVGInterval = 0;
int pulsesPerXMITperiod = 0;
volatile float CPM=0, Meters=0;
//CPM:cloth per minute
//CLM:cloth last minute
///////////Counter variables end

bool debugging = false; //if password is ok, this is true
bool netOK = false;     //checks internet is alive

bool ClothPresent = true; //check if cloth is present
const int ClothSensorPin = A1;
int clothThreshold=200; //Update this vaule at installation
int clothReading; //Not using cloth sensor

const int TemperaturePin = A0;
float Temperature;

const int openDetect = 3;
bool BoxOpen = false;
const unsigned int DataupdateFreq = 30000; //update every 30s
const unsigned int DataupdateFreq_SD = 60000; //update every 60s on SD


String debugPass = String("amethyst");
String str; //for storing input password

Sd2Card card;
SdVolume volume;
SdFile root;
File Datalog;
const int chipSelect = 53;

HardwareSerial & debug    = Serial;
HardwareSerial & esp8266  = Serial1;

const char DeviceID[] PROGMEM = "Blk1"; 
const char APIKey[]PROGMEM = "6359147591"; 
//const char DeviceID[] PROGMEM = "Haj1"; 
//const char APIKey[]PROGMEM = "6359199004"; 

const char* const Device_API[] PROGMEM = {DeviceID,APIKey};

//Use GMT time(2:30AM -> 8:00AM)
const int ShiftStartHour = 2;
const int ShiftStartMinute = 30 + 2 ;
//14:30 ->20:00(8:00PM)
const int ShiftEndHour = 14;
const int ShiftEndMinute = 30 + 2 ;

int shiftNow=0;
int preShift=0;
int eepromShift;
int eepromShiftAdd = 100;

//AT commands stored in flash
const char at[] PROGMEM=            "AT";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char rst[] PROGMEM =          "AT+RST";
const char cwjap[] PROGMEM =        "AT+CWJAP=\"OFFICE\",\"shdp@2016\"";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"blackelectronic.cloudapp.net\",8081";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";
const char cwdhcp[] PROGMEM =       "AT+CWDHCP=1,1";
const char staticIP[] PROGMEM =     "AT+CIPSTA=\"192.168.1.124\"";

//pointers to AT commands
const char*  AT PROGMEM = at;
const char*  CIPSTATUS PROGMEM = cipstatus;
const char*  RESET PROGMEM = rst;
const char*  CWJAP PROGMEM = cwjap;
const char*  CIPSTART PROGMEM = cipstart;
const char*  CIPCLOSE PROGMEM = cipclose;
const char*  CIFSR PROGMEM = cifsr;
const char*  CIPMUX PROGMEM = cipmux;
const char*  CWDHCP PROGMEM = cwdhcp;
const char*  STATICIP PROGMEM = staticIP;

const char POST[] PROGMEM =           "POST /api/service/InsertLog HTTP/1.1"; 
const char HOST[] PROGMEM =           "Host: blackelectronic.cloudapp.net:8081"; 
const char contentType[] PROGMEM =    "Content-Type: application/json"; 
const char contentLength[] PROGMEM =  "Content-Length: "; 

const char* const POSTMethod[] PROGMEM = {POST,HOST,contentType,contentLength};
String GET = "GET /api/service/GET?APIKey=  &DeviceCode= HTTP/1.0 ";
char cmdBuffer[100];
int postlen = 0;
int connection;

//Date Time
tmElements_t tmx;
bool RTCUpdatedFlag= false; //true updated
int Restarted = 1;  //RTC update on each reboot
int getTimefromNW = 0;
long sentTime; //use for various waiting while loops

//For RTC update, updates every month from nw
int NewMonth;
int Month;

//For eeprom save each hour
int NewHour, Hour;

int fileSent = 1; //0 not sent, 1 sent
int cipstartStatus;//main


unsigned long entries; //entries in SD log file
unsigned long sent_entries; //

bool trasnfer_in_progress = false; 

bool SDcardPresent = false; //Not used in program yet
const int CardDetect =49; 

const int RS = 12;
const int E  = 11;
const int D4 = 10;
const int D5 = 9;
const int D6 = 8;
const int D7 = 7;

LiquidCrystal lcd(RS,E,D4,D5,D6,D7);

//Function Declarations
String createJSON(void);
void sendData(int updateRTC);
byte countDigits(int num);
tmElements_t * string_to_tm(tmElements_t *tme, char *str);
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug);
int8_t connStatus(void);
bool netConnected(void);
void save2SD(void);
void sendfromSD(void);
unsigned long EEPROM_Read_Counter();
void EEPROM_Write_Counter(unsigned long counterNow);
unsigned long EEPROM_Read_ULong(int address);
void EEPROM_Write_ULong(int address, unsigned long data);
void readBoxTemp(void);
int getDatafromServer(void);
 
void setup(){
  lcd.begin(16, 2); 
  lcd.setCursor(0, 0);
  lcd.print("BlackElectronics");
  lcd.setCursor(0, 1);
  lcd.print("WiFi Prod. Meter");

  pinMode(openDetect,INPUT);
  pinMode(CardDetect,INPUT_PULLUP);
  
  debug.begin(115200);
  esp8266.begin(115200);
  debug.println(F("---------------------"));
  debug.println(F("Black Electronics"));
  debug.println(F("WiFi Production Meter"));
  debug.println(F("---------------------"));
  debug.println(F("Please Enter Password:"));
  delay(3000);
  //Read password from user
  if(debug.available()>0){
    while(debug.available()){
      str = Serial.readStringUntil('\n');
    }
  }
  str.trim();//removes \n
  if(str == debugPass){ //
    debugging = true;
    debug.println(F("ok"));
  }
  else{
    debugging = true;
    debug.println(F("Timeout"));
  }
  //SD setup
  pinMode(chipSelect, OUTPUT); //53 is chip select
  if (!SD.begin(chipSelect)) {
    if(debugging) debug.println(F("\r\nInitialization failed!"));
    SDcardPresent = false;
    return;
  }
  if(debugging) debug.println(F("\r\nInitialization done."));
  SDcardPresent = true;

  //ESP setup
  postlen = sizeof(POST) + sizeof(HOST) + sizeof(contentType) + sizeof(contentLength) + 7; //calcuate number for cipsend
  sendATcommand("AT","OK",1000,debugging);
  //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(STATICIP))); 
  //sendATcommand(cmdBuffer,"OK",3000,debugging); //WILL PRINT 0.0.0.0 IF NOT CONNECTED
  //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CWDHCP))); 
  //sendATcommand(cmdBuffer,"OK",3000,debugging); //WILL PRINT 0.0.0.0 IF NOT CONNECTED

  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIFSR))); 
  sendATcommand(cmdBuffer,"OK",3000,debugging); //WILL PRINT 0.0.0.0 IF NOT CONNECTED

  pinMode(LED, OUTPUT); 
  //read out contents of SD card
  if(SD.exists("LOG.TXT")){
    fileSent = 0; //File not sent flag
    Datalog = SD.open("LOG.TXT");
    while(Datalog.available()){
      if (Datalog.read() == '\r'){ //start of new dataline
        entries++; //count jsons entries not sent
        }
    }
    if(debugging){
      debug.print(F("No. of Entries: "));debug.println(entries);
    }
    entries = 0;
    Datalog.close();
  }
  
  unsigned long savedCounter = EEPROM_Read_Counter();
  if (savedCounter <=0) savedCounter = 1; //avoid division by 0
  PulseCounterVolatile = PulseCounter = PULSECOUNTLASTMINUTEMARK = savedCounter;
  //Calulate Meters from saved Counter
  Meters = ((float)PulseCounter)/PULSESPERMETER;
  attachInterrupt(digitalPinToInterrupt(INTERRUPTPIN), pulseCounterInterrupt, RISING);

  RTC.read(tmx);
  NewMonth = Month = tmx.Month;
  NewHour = Hour = tmx.Hour;
  //Determine which shift we are in
  //if(tmx.Hour <= ShiftHour && tmx.Minute <ShiftMinute){
  if(ShiftStartHour < tmx.Hour && tmx.Hour < ShiftEndHour){
    shiftNow = 1;
    preShift = 1;
  }
  else{
    shiftNow = 2;
    preShift = 2;
  }
  //read eeprom for shift data
  eepromShift = EEPROM.read(eepromShiftAdd);
  if(eepromShift != preShift){
    preShift = eepromShift;
    if(debugging) debug.println("---Shift changed");
  }
  if(debugging) debug.print(F("Shift: "));debug.println(shiftNow);
  //Shift times
  if(debugging) {
    sprintf(cmdBuffer,"Shift Time: %d:%d(GMT)",ShiftStartHour,ShiftStartMinute);
    debug.println(cmdBuffer);
    debug.print(F("DeviceID: "));
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[0]))); 
    debug.println(cmdBuffer);
  }
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Shift ");lcd.print(shiftNow);
  getDatafromServer();
}////////////////////////////////////////////////////setup end

void loop(){  
    if (debug.available())
      esp8266.write(debug.read());
    if (esp8266.available())
      debug.write(esp8266.read());
}//loop end
//////////////////////////////////////////////////////////

//Functions
int getDatafromServer(void){
  int GETlen;
  lcd.setCursor(0,1);
  lcd.print("Connecting");
  if(netOK || true){//remove true
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPCLOSE))); 
    sendATcommand(cmdBuffer,"OK",2000,debugging);
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTART)));
    sendATcommand(cmdBuffer,"OK",7000,debugging);    
    //sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[1]))); // Necessary casts and dereferencing, just copy.
    String GET = "GET /api/service/GET?APIKey=";
    GET += cmdBuffer;
    GET += "&DeviceCode=";
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[0]))); // Necessary casts and dereferencing, just copy.
    GET += cmdBuffer;
    GET += " HTTP/1.0";
    GETlen = GET.length() + 4; //79
    sprintf(cmdBuffer, "AT+CIPSEND=%d", GETlen);
    sendATcommand(cmdBuffer,">",1000,debugging);
    esp8266.print(GET);
    esp8266.println();
    esp8266.println();
    //parse data here
}
    //send get request
    //do your thing
    //GET METERS FROM SERVERs
  //AT+CIPSTART="TCP","blackelectronic.cloudapp.net",8081
  //AT+CIPSEND=79
  //GET /api/service/GET?APIKey=6359147591&DeviceCode=Blk1 HTTP/1.0 


}
void readBoxTemp(void){ //read cloth sensor,box open sw and temperature
  if(tmx.Year >= 70 || tmx.Year <=40){ //Time glitch work around
    Restarted = 1; //update again
  }
  clothReading = analogRead(ClothSensorPin);
  if(clothReading >= clothThreshold){
    ClothPresent = true;
    lcd.setCursor(0,1);
    lcd.print(Meters);lcd.print("m,CPM:");lcd.print(CPM);
  }
  else{
    ClothPresent = false;
  }
  BoxOpen = !(digitalRead(openDetect)); //0 for closed, 1 open
  int reading = analogRead(TemperaturePin);  
  Temperature = reading * 0.48828125;
}

void sendfromSD(void){
  debug.println(F("\r\nSending from SD"));
  if(!trasnfer_in_progress){
    if(SD.exists("LOG.TXT")){
      Datalog = SD.open("LOG.TXT");
      Datalog.seek(0);
      while(Datalog.available()){
        if (Datalog.read() == '\r'){ //start of new dataline
          entries++; //count jsons 
        }
      }
      debug.print(F("\r\nTotal Entries on SD: "));debug.println(entries);
    Datalog.seek(0); //go back to begining of file or close and reopen
    }
    trasnfer_in_progress = true;
    
  }
  //check eeprom for sent entries
  //copy to sententries variable in RAM
  //if present run for loop to jump to appropriate location to send
  if (sent_entries < entries){
    sendData(2);  //2 reads json from log file
    sent_entries++;
    debug.print(F("\r\nSent Entries: "));debug.println(sent_entries);
    //write sent entries in eeprom 
   // EEPROMWritelong(1,sent_entries);
  }
  else if(sent_entries >= entries && fileSent == 0 ){
    //clean up variables
    debug.println(F("\r\nLog file sent and removed"));
    fileSent = 1;
    trasnfer_in_progress = false;
    entries = 0;
    sent_entries = 0;
    SD.remove("LOG.TXT");
  }  
}

void save2SD(void){
  fileSent = 0;
  Datalog = SD.open("LOG.TXT",FILE_WRITE);//write or append
  if(Datalog){ //if file open
    String machine_data = createJSON();
    Datalog.println(machine_data); //write to file
    Datalog.close(); //close file to save the contents
    debug.println(F("\r\nData saved to SD"));
    if(debugging)debug.println(machine_data);
  }
  else
    debug.println(F("\r\nError opening file"));
    //Display on LCD to change SD card
}

byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
}

void sendData(int command){
  //command = 0 -> Normal send
  //command = 1 -> update RTC
  //command = 2 -> send from SD card
  char date_string[32];
  char *datep = &date_string[0];

  String json;
  int jsonLen; //length of json string
  int jsonlenlen; //jsonlen is 120, jsonlenlen is 3
  int cipsendLen; //total of cipsend
    
    if(command == 2){ //sending from SD card
      json = Datalog.readStringUntil('\n');
    }
    else{
      json = createJSON();  
    }
    if(debugging)debug.println(json);
    jsonLen = json.length();
    jsonlenlen = countDigits(jsonLen);
    cipsendLen = jsonLen + jsonlenlen + postlen ;
    sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
    sendATcommand(cmdBuffer,">",1000,debugging);
    for (int i = 0; i <=2 ; i++){
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
      esp8266.println(cmdBuffer);
      delay(50);
    }
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
    esp8266.print(cmdBuffer);
    esp8266.println(jsonLen);
    esp8266.println();
    delay(50);
    esp8266.print(json);
    sendATcommand(" ","SEND OK",5000,debugging); //consider removing this

  //Parse the header for date
  if(command == 1 && !(RTCUpdatedFlag)){
    //update only if data is available from the network
     //Parse the returned header & web page. Looking for 'Date' line in header
    if (esp8266.find("Date: ")) //get the date line from the http header (for example)
    {
      int i;
      for (i = 0; i < 31; i++) //31 this should capture the 'Date: ' line from the header
      {
        if (esp8266.available())  //new characters received?
        {
          char c = esp8266.read(); //print to console
          //debug.write(c);
          date_string[i] = c;
        }
        else i--;  //if not, keep going round loop until we've got all the characters
      }
    }
    string_to_tm(&tmx, date_string);
    //update RTC
    if(tmx.Year <= 70 && tmx.Year>=40){ //Time glitch work around
      if(RTC.write(tmx)){
        RTCUpdatedFlag = true;
        if(debugging) debug.println(F("\r\nRTC Updated"));
      }
    } 
    if (RTC.read(tmx)) {
      if(debugging) {
        debug.print(F("Ok, Time = "));
        debug.print(tmx.Hour);
        debug.write(':');
        debug.print(tmx.Minute);
        debug.write(':');
        debug.print(tmx.Second);
        debug.print(F(", Date (D/M/Y) = "));
        debug.print(tmx.Day);
        debug.write('/');
        debug.print(tmx.Month);
        debug.write('/');
        debug.print(tmYearToCalendar(tmx.Year));
        debug.println();
      }
    } 
  }//RTC update end  
}//send data end

int8_t connStatus(void){
    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100); 
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTATUS))); 
    esp8266.println(cmdBuffer); //send cipstatus command
    x = 0;
    previous = millis();
    // this loop waits for the answer
    do{
      if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
          response[x] = esp8266.read();
          if(debugging){
              debug.print(response[x]);
          }
          x++;
          if      (strstr(response, "STATUS:2") != NULL)    //connected to AP
          {
              answer = 2;
          }
          else if (strstr(response, "STATUS:3") != NULL)    //connected to webserver 
          {
              answer = 3;
          }
          else if (strstr(response, "STATUS:4") != NULL)    //disconnected from webserver
          {
              answer = 4;
          }
          else if (strstr(response, "STATUS:5") != NULL)    //not connected to ap
          {
              answer = 5;
          }
      }
    }while((answer == 0) && ((millis() - previous) < 5000));    // Waits for the asnwer with time out

    return answer;
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command 
    }
    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
      if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
          response[x] = esp8266.read();
          if(isDebug && debugging){
              debug.print(response[x]);
          }
          x++;
          if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 1;
          }
          else if(strstr(response, "DNS Fail") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 2;
              break;
          }
          else if(strstr(response, "no ip") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 3;
              break;
          }
          else if(strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 0;
              break;
          }
      }
    readBoxTemp();  //read inputs
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out
  return answer;
}

String createJSON(void){

  String json;
  //refreshCounter();
  readBoxTemp();
  RTC.read(tmx);
  json = "{";
  json += "\"DT\":\"";
  if(tmx.Day > 0 && tmx.Day <10){
    json += "0";
  }
  json += tmx.Day;
  json += "-";
  if(tmx.Month >0 && tmx.Month <10){
    json += "0";
  }
  json += tmx.Month;
  json += "-";
  json += tmYearToCalendar(tmx.Year);

  json += " ";
  if(tmx.Hour >=0 && tmx.Hour <10){
    json += "0";
  }
  json += tmx.Hour;
  json += ":";
  if(tmx.Minute >=0 && tmx.Minute <10){
    json += "0";
  }
  json += tmx.Minute;
  json += ":";
  if(tmx.Second >=0 && tmx.Second <10){
    json += "0";
  }
  json += tmx.Second;
  json +="\",\"DeviceID\":\"";
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[0]))); // Necessary casts and dereferencing, just copy.
  json += cmdBuffer; //device id
  json += "\",\"Meters\":";
  noInterrupts();
  json += Meters;
  json += ",\"MeterPerMinute\":";
  json += CPM;
  interrupts(); 
  json += ",\"ClothPresent\":";
  json += ClothPresent;
  json += ",\"Temperature\":";
  json += Temperature;
  json += ",\"BoxOpen\":";
  json += BoxOpen;
  json += ",\"APIKey\":\"";
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[1]))); // Necessary casts and dereferencing, just copy.
  json += cmdBuffer; //APIkey
  json += "\"}";

  return json;
}

tmElements_t * string_to_tm(tmElements_t *tme, char *str) {
  // Sat, 28 Mar 2015 13:53:38 GMT

  const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  char *r, *i, *t;
  r = strtok_r(str, " ", &i);

  r = strtok_r(NULL, " ", &i);
  tme->Day = atoi(r);

  r = strtok_r(NULL, " ", &i);
  for (int i = 0; i < 12; i++) {
    if (!strcmp(months[i], r)) {
      tme->Month = i + 1;
      break;
    }
  }
  r = strtok_r(NULL, " ", &i);
  tme->Year = atoi(r) - 1970;

  r = strtok_r(NULL, " ", &i);
  t = strtok_r(r, ":", &i);
  tme->Hour = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Minute = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Second = atoi(t);

  return tme;
}

unsigned long EEPROM_Read_Counter()
{
  return EEPROM_Read_ULong(EEPROM_Read_ULong(COUNTERADDR));
}

void EEPROM_Write_Counter(unsigned long counterNow)
{
  if (counterNow == EEPROM_Read_Counter())
  {
    debug.print("{EEPROM-SKIP(no changes)}");
    return; //skip if nothing changed
  }
  
  debug.print("{EEPROM-SAVE(");
  debug.print(EEPROM_Read_ULong(COUNTERADDR));
  debug.print(")=");
  debug.print(PulseCounter);
  debug.print("}");
    
  unsigned long CounterAddr = EEPROM_Read_ULong(COUNTERADDR);
  if (CounterAddr == COUNTERADDRBASE+8*(COUNTEREEPROMSLOTS-1))
    CounterAddr = COUNTERADDRBASE;
  else CounterAddr += 8;
  
  EEPROM_Write_ULong(CounterAddr, counterNow);
  EEPROM_Write_ULong(COUNTERADDR, CounterAddr);
}

unsigned long EEPROM_Read_ULong(int address)
{
  unsigned long temp;
  for (byte i=0; i<8; i++)
    temp = (temp << 8) + EEPROM.read(address++);
  return temp;
}

void EEPROM_Write_ULong(int address, unsigned long data)
{
  for (byte i=0; i<8; i++)
  {
    EEPROM.write(address+7-i, data);
    data = data >> 8;
  }
}

void pulseCounterInterrupt()
{
  noInterrupts();
  ledState = !ledState;
  if(ClothPresent){
    PulseCounterVolatile++;  
  }
  digitalWrite(LED, ledState);
  NOW = millis();

  //remember how long between pulses (sliding window)
  TIMESTAMP_pulse_prev = TIMESTAMP_pulse_curr;
  TIMESTAMP_pulse_curr = NOW;
  
  if (TIMESTAMP_pulse_curr - TIMESTAMP_pulse_prev > CPMTHRESHOLD)
    //more than 'GPMthreshold' seconds passed since last pulse... resetting GPM
    pulsesPerXMITperiod=pulseAVGInterval=0;
  else
  {
    pulsesPerXMITperiod++;
    pulseAVGInterval += TIMESTAMP_pulse_curr - TIMESTAMP_pulse_prev;
  }
  PulseCounter = PulseCounterVolatile;
  Meters = ((float)PulseCounter)/PULSESPERMETER;
  CPM = pulseAVGInterval > 0 ? 60.0 * 1000 * (1.0/PULSESPERMETER)/(pulseAVGInterval/pulsesPerXMITperiod): 0;
  interrupts();
}
