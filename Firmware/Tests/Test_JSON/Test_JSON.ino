//Production Meter8IN1
//Date:18-MAR-16
//arduino 1.0.6

//TODO
//Hangs with busy p... botch and do hard reset, use debug pins
//Get latest value from server Testing
//cycle display on lcd
//change json
//use data struct


/*
Things to update at installation
  FORMAT sd CARD
  Run SetRTC with full compailation
  Run clearSD and eeprom program
  SET AT+CWMODE=1
  join wifi network
  CIPStart post host
  DeviceID
  API key
  change Shift time
  Pulsespermeter
  CPM threshold time
  clothThreshold
  remove debugging true in setup 
*/

#include <avr/wdt.h>
#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <SD.h>        
#include <EEPROM.h>
#include <LiquidCrystal.h>

///Counter Variables
//RATIOS
#define PULSESPERMETER1    1 //how many pulses from sensor equal 1 meter of cloth(100/(Pi*Dcm)
#define PULSESPERMETER2    1 
#define PULSESPERMETER3    1 
#define PULSESPERMETER4    1 
#define PULSESPERMETER5    1 
#define PULSESPERMETER6    1 
#define PULSESPERMETER7    1 
#define PULSESPERMETER8    1 
//INTERRUPT PINS
#define PROXY1   A8  
#define PROXY2   A9  
#define PROXY3   A10  
#define PROXY4   A11  
#define PROXY5   A12  
#define PROXY6   A13  
#define PROXY7   A14  
#define PROXY8   A15  

#define NOT_AN_INTERRUPT  -1 // silent the compiler on 1.0.6

volatile unsigned long PulseCounterVolatile1 = 0; // use volatile for shared variables
volatile unsigned long PulseCounterVolatile2 = 0;
volatile unsigned long PulseCounterVolatile3 = 0;
volatile unsigned long PulseCounterVolatile4 = 0;
volatile unsigned long PulseCounterVolatile5 = 0;
volatile unsigned long PulseCounterVolatile6 = 0;
volatile unsigned long PulseCounterVolatile7 = 0;
volatile unsigned long PulseCounterVolatile8 = 0;

unsigned long NOW = 0;
unsigned long PulseCounter1 = 0;
unsigned long PulseCounter2 = 0;
unsigned long PulseCounter3 = 0;
unsigned long PulseCounter4 = 0;
unsigned long PulseCounter5 = 0;
unsigned long PulseCounter6 = 0;
unsigned long PulseCounter7 = 0;
unsigned long PulseCounter8 = 0;

byte COUNTEREEPROMSLOTS = 10;
unsigned long COUNTERADDRBASE = 8; //address in EEPROM that points to the first possible slot for a counter
unsigned long COUNTERADDR = 0;     //address in EEPROM that points to the latest Counter in EEPROM

volatile float Meters1=0;
volatile float Meters2=0;
volatile float Meters3=0;
volatile float Meters4=0;
volatile float Meters5=0;
volatile float Meters6=0;
volatile float Meters7=0;
volatile float Meters8=0;

float preMeters1 = 0;
float preMeters2 = 0;
float preMeters3 = 0;
float preMeters4 = 0;
float preMeters5 = 0;
float preMeters6 = 0;
float preMeters7 = 0;
float preMeters8 = 0;

const int SaveCounterAddress1 = 0;
const int SaveCounterAddress2 = 8;
const int SaveCounterAddress3 = 16;
const int SaveCounterAddress4 = 24;
const int SaveCounterAddress5 = 32;
const int SaveCounterAddress6 = 40;
const int SaveCounterAddress7 = 48;
const int SaveCounterAddress8 = 56;
///////////Counter variables end

bool ClothPresent1 = true; //check if cloth is present
bool ClothPresent2 = true; //check if cloth is present
bool ClothPresent3 = true; //check if cloth is present
bool ClothPresent4 = true; //check if cloth is present
bool ClothPresent5 = true; //check if cloth is present
bool ClothPresent6 = true; //check if cloth is present
bool ClothPresent7 = true; //check if cloth is present
bool ClothPresent8 = true; //check if cloth is present

const int ClothSensorPin1 = A0;
const int ClothSensorPin2 = A1;
const int ClothSensorPin3 = A2;
const int ClothSensorPin4 = A3;
const int ClothSensorPin5 = A4;
const int ClothSensorPin6 = A5;
const int ClothSensorPin7 = A6;
const int ClothSensorPin8 = A7;

const int clothThreshold1=200; //Update this vaule at installation
const int clothThreshold2=200; 
const int clothThreshold3=200; 
const int clothThreshold4=200; 
const int clothThreshold5=200; 
const int clothThreshold6=200; 
const int clothThreshold7=200; 
const int clothThreshold8=200; 

int clothReading1; 
int clothReading2;
int clothReading3;
int clothReading4;
int clothReading5;
int clothReading6;
int clothReading7;
int clothReading8; 
///////////////////////////////////
/*
struct Proxy
{
  int pulsePerMeter;
  const int ProxyPin;
  volatile unsigned long PulseCounterVolatile;
  volatile float Meter;
  float preMeters;
  unsigned long PulseCounter;
};

struct ClothSensor
{
  bool clothPresent = true;
  int clothThreshold;
  int clothReading;
  const int sensorPin;
};
*/
bool debugging = false; //if password is ok, this is true
bool netOK = false;     //checks internet is alive

const unsigned int DataupdateFreq = 30000; //update every 30s
const unsigned int DataupdateFreq_SD = 600000; //update every 10 minutes on SD(144 entries per 24 hrs)

String debugPass = String("amethyst");
String str; //for storing input password

Sd2Card card;
SdVolume volume;  
SdFile root;
File Datalog;
const int chipSelect = 53;

HardwareSerial & debug    = Serial;
HardwareSerial & esp8266  = Serial1;

const char DeviceID[] PROGMEM = "Blk1"; 
const char APIKey[]PROGMEM = "6359147591";

const char* const Device_API[] PROGMEM = {DeviceID,APIKey};

//Use GMT time(2:30AM -> 8:00AM)
const int ShiftStartHour = 2;
const int ShiftStartMinute = 30 + 2 ;
//14:30 ->20:00(8:00PM)
const int ShiftEndHour = 14;
const int ShiftEndMinute = 30 + 2 ;

int shiftNow=0;
int preShift=0;
int eepromShift;
const int eepromShiftAdd = 64;

//AT commands stored in flash
const char at[] PROGMEM=            "AT";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char rst[] PROGMEM =          "AT+RST";
const char cwjap[] PROGMEM =        "AT+CWJAP=\"ProductionMeter\",\"blackelectronics@123\"";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"blackelectronic.cloudapp.net\",8081";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";
const char cwdhcp[] PROGMEM =       "AT+CWDHCP=1,1";
const char staticIP[] PROGMEM =     "AT+CIPSTA=\"192.168.1.124\"";

const char POST[] PROGMEM =           "POST /api/service/InsertLog HTTP/1.1"; 
const char HOST[] PROGMEM =           "Host: blackelectronic.cloudapp.net:8081"; 
const char contentType[] PROGMEM =    "Content-Type: application/json"; 
const char contentLength[] PROGMEM =  "Content-Length: "; 

//pointers to AT commands
const char*  AT PROGMEM = at;
const char*  CIPSTATUS PROGMEM = cipstatus;
const char*  RESET PROGMEM = rst;
const char*  CWJAP PROGMEM = cwjap;
const char*  CIPSTART PROGMEM = cipstart;
const char*  CIPCLOSE PROGMEM = cipclose;
const char*  CIFSR PROGMEM = cifsr;
const char*  CIPMUX PROGMEM = cipmux;
const char*  CWDHCP PROGMEM = cwdhcp;
const char*  STATICIP PROGMEM = staticIP;
const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};

char cmdBuffer[100];
int postlen = 0;
int connection;

//Date Time
tmElements_t tmx;
bool RTCUpdatedFlag= false; //true updated
int Restarted = 1;  //RTC update on each reboot
int getTimefromNW = 0;
long sentTime; //use for various waiting while loops

//For RTC update, updates every month from nw
int NewMonth;
int Month;

//For eeprom save each hour
int NewHour, Hour;

int fileSent = 1; //0 not sent, 1 sent
int cipstartStatus;//main

unsigned long entries; //entries in SD log file
unsigned long sent_entries; //

bool trasnfer_in_progress = false; 

const int openDetect = 3;
bool BoxOpen = false;

bool SDcardPresent = false; //Not used in program yet
const int CardDetect =49; 

const int RS = 12;
const int E  = 11;
const int D4 = 10;
const int D5 = 9;
const int D6 = 8;
const int D7 = 7;

LiquidCrystal lcd(RS,E,D4,D5,D6,D7);

//Function Declarations
String createJSON(void);
void sendData(int updateRTC);
byte countDigits(int num);
tmElements_t * string_to_tm(tmElements_t *tme, char *str);
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug);
int8_t connStatus(void);
bool netConnected(void);
void save2SD(void);
void sendfromSD(void);
unsigned long EEPROM_Read_ULong(int address);
void EEPROM_Write_ULong(int address, unsigned long data);
void updateLCD(void);
void SaveCountersEEPROM(void);

String json;
void setup(){

  debug.begin(115200);
  debug.println(F("---------------------"));
  RTC.read(tmx);
  json = createJSON();
  debug.println(json);
  debug.println(F("---------------------"));
 
}////////////////////////////////////////////////////setup end

void loop(){
 
delay(5000);
}//loop end
//////////////////////////////////////////////////////////

///Functions
void updateLCD(void){
//updates lcd and reads cloth sensor  

  BoxOpen = !(digitalRead(openDetect)); //0 for closed, 1 open
  
  clothReading1 = analogRead(ClothSensorPin1);
  clothReading2 = analogRead(ClothSensorPin2);
  clothReading3 = analogRead(ClothSensorPin3);
  clothReading4 = analogRead(ClothSensorPin4);
  clothReading5 = analogRead(ClothSensorPin5);
  clothReading6 = analogRead(ClothSensorPin6);
  clothReading7 = analogRead(ClothSensorPin7);
  clothReading8 = analogRead(ClothSensorPin8);

  ClothPresent1 = clothReading1 >= clothThreshold1 ? true : false;
  ClothPresent2 = clothReading2 >= clothThreshold2 ? true : false;
  ClothPresent3 = clothReading3 >= clothThreshold3 ? true : false;
  ClothPresent4 = clothReading4 >= clothThreshold4 ? true : false;
  ClothPresent5 = clothReading5 >= clothThreshold5 ? true : false;
  ClothPresent6 = clothReading6 >= clothThreshold6 ? true : false;
  ClothPresent7 = clothReading7 >= clothThreshold7 ? true : false;
  ClothPresent8 = clothReading8 >= clothThreshold8 ? true : false;

  //cycle lcd display per secondn auto and button

}
void sendfromSD(void){
  debug.println(F("\r\nSending from SD"));
  if(!trasnfer_in_progress){
    if(SD.exists("LOG.TXT")){
      Datalog = SD.open("LOG.TXT");
      Datalog.seek(0);
      while(Datalog.available()){
        if (Datalog.read() == '\r'){ //start of new dataline
          entries++; //count jsons 
        }
      }
      debug.print(F("\r\nTotal Entries on SD: "));debug.println(entries);
    Datalog.seek(0); //go back to begining of file or close and reopen
    }
    trasnfer_in_progress = true;
    
  }
  //check eeprom for sent entries
  //copy to sententries variable in RAM
  //if present run for loop to jump to appropriate location to send
  if (sent_entries < entries){
    sendData(2);  //2 reads json from log file
    sent_entries++;
    debug.print(F("\r\nSent Entries: "));debug.println(sent_entries);
    //write sent entries in eeprom 
   // EEPROMWritelong(1,sent_entries);
  }
  else if(sent_entries >= entries && fileSent == 0 ){
    //clean up variables
    debug.println(F("\r\nLog file sent and removed"));
    fileSent = 1;
    trasnfer_in_progress = false;
    entries = 0;
    sent_entries = 0;
    SD.remove("LOG.TXT");
  }  
}

void save2SD(void){
  fileSent = 0;
  Datalog = SD.open("LOG.TXT",FILE_WRITE);//write or append
  if(Datalog){ //if file open
    String machine_data = createJSON();
    Datalog.println(machine_data); //write to file
    Datalog.close(); //close file to save the contents
    debug.println(F("\r\nData saved to SD"));
    if(debugging)debug.println(machine_data);
  }
  else
    debug.println(F("\r\nError opening file"));
    //Display on LCD to change SD card
}

byte countDigits(int num){
  byte count=0;
  while(num){
    num=num/10;
    count++;
  }
  return count;
}

void sendData(int command){
  //command = 0 -> Normal send
  //command = 1 -> update RTC
  //command = 2 -> send from SD card
  char date_string[32];
  char *datep = &date_string[0];

  String json;
  int jsonLen; //length of json string
  int jsonlenlen; //jsonlen is 120, jsonlenlen is 3
  int cipsendLen; //total of cipsend
    
    if(command == 2){ //sending from SD card
      json = Datalog.readStringUntil('\n');
    }
    else{
      json = createJSON();  
    }
    if(debugging)debug.println(json);
    jsonLen = json.length();
    jsonlenlen = countDigits(jsonLen);
    cipsendLen = jsonLen + jsonlenlen + postlen ;
    sprintf(cmdBuffer, "AT+CIPSEND=%d", cipsendLen);
   // sendATcommand(cmdBuffer,">",1000,debugging);
    for (int i = 0; i <=2 ; i++){
      strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
      esp8266.println(cmdBuffer);
      delay(50);
    }
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
    esp8266.print(cmdBuffer);
    esp8266.println(jsonLen);
    esp8266.println();
    delay(50);
    esp8266.print(json);
    //sendATcommand(" ","SEND OK",5000,debugging); //consider removing this

  //Parse the header for date
  if(command == 1 && !(RTCUpdatedFlag)){
    //update only if data is available from the network
     //Parse the returned header & web page. Looking for 'Date' line in header
    if (esp8266.find("Date: ")) //get the date line from the http header (for example)
    {
      int i;
      for (i = 0; i < 31; i++) //31 this should capture the 'Date: ' line from the header
      {
        if (esp8266.available())  //new characters received?
        {
          char c = esp8266.read(); //print to console
          //debug.write(c);
          date_string[i] = c;
        }
        else i--;  //if not, keep going round loop until we've got all the characters
      }
    }
    string_to_tm(&tmx, date_string);
    //update RTC
    if(tmx.Year <= 70 && tmx.Year>=40){ //Time glitch work around
      if(RTC.write(tmx)){
        RTCUpdatedFlag = true;
        if(debugging) debug.println(F("\r\nRTC Updated"));
      }
    } 
    if (RTC.read(tmx)) {
      if(debugging) {
        debug.print(F("Ok, Time = "));
        debug.print(tmx.Hour);
        debug.write(':');
        debug.print(tmx.Minute);
        debug.write(':');
        debug.print(tmx.Second);
        debug.print(F(", Date (D/M/Y) = "));
        debug.print(tmx.Day);
        debug.write('/');
        debug.print(tmx.Month);
        debug.write('/');
        debug.print(tmYearToCalendar(tmx.Year));
        debug.println();
      }
    } 
  }//RTC update end  
}//send data end

int8_t connStatus(void){
    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100); 
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    strcpy_P(cmdBuffer, (char*)pgm_read_word(&(CIPSTATUS))); 
    esp8266.println(cmdBuffer); //send cipstatus command
    x = 0;
    previous = millis();
    // this loop waits for the answer
    do{
      if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
          response[x] = esp8266.read();
          if(debugging){
              debug.print(response[x]);
          }
          x++;
          if      (strstr(response, "STATUS:2") != NULL)    //connected to AP
          {
              answer = 2;
          }
          else if (strstr(response, "STATUS:3") != NULL)    //connected to webserver 
          {
              answer = 3;
          }
          else if (strstr(response, "STATUS:4") != NULL)    //disconnected from webserver
          {
              answer = 4;
          }
          else if (strstr(response, "STATUS:5") != NULL)    //not connected to ap
          {
              answer = 5;
          }
      }
    }while((answer == 0) && ((millis() - previous) < 5000));    // Waits for the asnwer with time out

    return answer;
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int Timeout, boolean isDebug){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( esp8266.available() > 0) esp8266.read();    // Clean the input cmdBuffer
    esp8266.flush();
    if (ATcommand[0] != '\0')
    {
    esp8266.println(ATcommand);    // Send the AT command 
    }
    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
      if(esp8266.available() != 0){    // if there are data in the UART input cmdBuffer, reads it and checks for the asnwer
          response[x] = esp8266.read();
          if(isDebug && debugging){
              debug.print(response[x]);
          }
          x++;
          if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 1;
          }
          else if(strstr(response, "DNS Fail") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 2;
              break;
          }
          else if(strstr(response, "no ip") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 3;
              break;
          }
          else if(strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
          {
              answer = 0;
              break;
          }
      }
    updateLCD();  
    }while((answer == 0) && ((millis() - previous) < Timeout));    // Waits for the asnwer with time out
  return answer;
}

String createJSON(void){

  String json;
  //refreshCounter();
  updateLCD();
 RTC.read(tmx);
  json = "{";
  json += "\"DT\":\"";
  if(tmx.Day > 0 && tmx.Day <10){
    json += "0";
  }
  json += tmx.Day;
  json += "-";
  if(tmx.Month >0 && tmx.Month <10){
    json += "0";
  }
  json += tmx.Month;
  json += "-";
  json += tmYearToCalendar(tmx.Year);

  json += " ";
  if(tmx.Hour >=0 && tmx.Hour <10){
    json += "0";
  }
  json += tmx.Hour;
  json += ":";
  if(tmx.Minute >=0 && tmx.Minute <10){
    json += "0";
  }
  json += tmx.Minute;
  json += ":";
  if(tmx.Second >=0 && tmx.Second <10){
    json += "0";
  }
  json += tmx.Second;
  json +="\",\"DeviceID\":\"";
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[0]))); // Necessary casts and dereferencing, just copy.
  json += cmdBuffer; //device id
//  {"DT":"19-03-2016 06:33:03","DeviceID":"Blk1","Meters":[123.8,1.8235,125.3,12.3,125.3,15.23,123.6,123.6],"BoxOpen":0,"APIKey":"635914759"}
  json += "\",\"Meters\":[";
  //no interrupes
  json += Meters1;
  json += ",";
  json += Meters2;
  json += ",";
  json += Meters3;
  json += ",";
  json += Meters4;
  json += ",";
  json += Meters5;
  json += ",";
  json += Meters6;
  json += ",";
  json += Meters7;
  json += ",";
  json += Meters8;
  json += "]";
  //interrupts
  json += ",\"BoxOpen\":";
  json += BoxOpen;
  json += ",\"APIKey\":\"";
  strcpy_P(cmdBuffer, (char*)pgm_read_word(&(Device_API[1]))); // Necessary casts and dereferencing, just copy.
  json += cmdBuffer; //APIkey
  json += "\"}";

  return json;
}

tmElements_t * string_to_tm(tmElements_t *tme, char *str) {
  // Sat, 28 Mar 2015 13:53:38 GMT

  const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  char *r, *i, *t;
  r = strtok_r(str, " ", &i);

  r = strtok_r(NULL, " ", &i);
  tme->Day = atoi(r);

  r = strtok_r(NULL, " ", &i);
  for (int i = 0; i < 12; i++) {
    if (!strcmp(months[i], r)) {
      tme->Month = i + 1;
      break;
    }
  }
  r = strtok_r(NULL, " ", &i);
  tme->Year = atoi(r) - 1970;

  r = strtok_r(NULL, " ", &i);
  t = strtok_r(r, ":", &i);
  tme->Hour = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Minute = atoi(t);

  t = strtok_r(NULL, ":", &i);
  tme->Second = atoi(t);

  return tme;
}

unsigned long EEPROM_Read_ULong(int address)
{
  unsigned long temp;
  for (byte i=0; i<8; i++)
    temp = (temp << 8) + EEPROM.read(address++);
  return temp;
}

void EEPROM_Write_ULong(int address, unsigned long data)
{
  for (byte i=0; i<8; i++)
  {
    EEPROM.write(address+7-i, data);
    data = data >> 8;
  }
}
void SaveCountersEEPROM(void){
  EEPROM_Write_ULong(SaveCounterAddress1,PulseCounter1);
  EEPROM_Write_ULong(SaveCounterAddress2,PulseCounter2);
  EEPROM_Write_ULong(SaveCounterAddress3,PulseCounter3);
  EEPROM_Write_ULong(SaveCounterAddress4,PulseCounter4);
  EEPROM_Write_ULong(SaveCounterAddress5,PulseCounter5);
  EEPROM_Write_ULong(SaveCounterAddress6,PulseCounter6);
  EEPROM_Write_ULong(SaveCounterAddress7,PulseCounter7);
  EEPROM_Write_ULong(SaveCounterAddress8,PulseCounter8);
}
/////////////////////////////
void pulseCounterInterrupt1()
{
  noInterrupts();
  if(ClothPresent1){
    PulseCounterVolatile1++;  
  }

  PulseCounter1 = PulseCounterVolatile1;
  Meters1 = ((float)PulseCounter1)/PULSESPERMETER1;

  interrupts();
}
void pulseCounterInterrupt2()
{
  noInterrupts();
  if(ClothPresent2){
    PulseCounterVolatile2++;  
  }

  PulseCounter2 = PulseCounterVolatile2;
  Meters2 = ((float)PulseCounter2)/PULSESPERMETER2;

  interrupts();
}
void pulseCounterInterrupt3()
{
  noInterrupts();
  if(ClothPresent3){
    PulseCounterVolatile3++;  
  }

  PulseCounter3 = PulseCounterVolatile3;
  Meters3 = ((float)PulseCounter3)/PULSESPERMETER3;

  interrupts();
}
void pulseCounterInterrupt4()
{
  noInterrupts();
  if(ClothPresent4){
    PulseCounterVolatile4++;  
  }

  PulseCounter4 = PulseCounterVolatile4;
  Meters4 = ((float)PulseCounter4)/PULSESPERMETER4;

  interrupts();
}
void pulseCounterInterrupt5()
{
  noInterrupts();
  if(ClothPresent5){
    PulseCounterVolatile5++;  
  }

  PulseCounter5 = PulseCounterVolatile5;
  Meters5 = ((float)PulseCounter5)/PULSESPERMETER5;

  interrupts();
}
void pulseCounterInterrupt6()
{
  noInterrupts();
  if(ClothPresent6){
    PulseCounterVolatile6++;  
  }

  PulseCounter6 = PulseCounterVolatile6;
  Meters6 = ((float)PulseCounter6)/PULSESPERMETER6;

  interrupts();
}
void pulseCounterInterrupt7()
{
  noInterrupts();
  if(ClothPresent7){
    PulseCounterVolatile7++;  
  }

  PulseCounter7 = PulseCounterVolatile7;
  Meters7 = ((float)PulseCounter7)/PULSESPERMETER7;

  interrupts();
}
void pulseCounterInterrupt8()
{
  noInterrupts();
  if(ClothPresent8){
    PulseCounterVolatile8++;  
  }

  PulseCounter8 = PulseCounterVolatile8;
  Meters8 = ((float)PulseCounter8)/PULSESPERMETER8;

  interrupts();
}