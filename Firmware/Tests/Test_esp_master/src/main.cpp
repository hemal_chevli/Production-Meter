/*
 *  This sketch sends data via HTTP GET requests to data.sparkfun.com service.
 *
 *  You need to get streamId and privateKey at data.sparkfun.com and paste them
 *  below. Or just customize this script to talk to other HTTP servers.
 *
 */

#include <ESP8266WiFi.h>

const char* ssid     = "Black Electronics";
const char* password = "blackcorp";


//Working example using HTTP
// http.begin("http://things.ubidots.com/api/v1.6/variables/_---change-with-your-var-id----_/values");
// http.addHeader("Content-Type", "application/json");
// http.addHeader("X-Auth-Token", "_----change-to-your-token---_");
// http.addHeader("NULL", "NULL");
// http.POST("{\"value\": 20 }");
// http.writeToStream(&Serial);
// http.end();

const char* url = "blackelectronic.cloudapp.net";
const char* host = "blackelectronic.cloudapp.net:8081";
const int httpPort = 8081;
String data = "{\"DT\":\"0-01-2048 00:00:11\",\"DeviceID\":\"Blk1\",\"Meters\":2.00,\"MeterPerMinute\":0.00,\"ClothPresent\":1,\"Temperature\":32.23,\"BoxOpen\":0,\"APIKey\":\"6359147591\"}";
void setup() {
  Serial.begin(115200);
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {


  Serial.print("connecting to ");
  Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;

  if (!client.connect(url, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  //const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"blackelectronic.cloudapp.net\",8081";
  // const char POST[] PROGMEM =           "POST /api/service/InsertLog HTTP/1.1";
  // const char HOST[] PROGMEM =           "Host: blackelectronic.cloudapp.net:8081";
  // const char contentType[] PROGMEM =    "Content-Type: application/json";
  // const char contentLength[] PROGMEM =  "Content-Length: ";


  // We now create a URI for the request
  String PostRequest = "POST /api/service/InsertLog HTTP/1.1\r\n";
  PostRequest += "Host: blackelectronic.cloudapp.net:8081\r\n";
  PostRequest += "Content-Type: application/json\r\n";
  PostRequest += "Content-Length: ";
  PostRequest += data.length();
  PostRequest +="\r\n\r\n" +data;

  Serial.println("Requesting URL: ");
  delay(10);
  Serial.println(PostRequest);

  // This will send the request to the server
  client.println(PostRequest);

  int timeout = millis() + 5000;
  while (client.available() == 0) {
    if (timeout - millis() < 0) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }

  Serial.println();
  Serial.println("closing connection");
  delay(30000);
}//loop end

/*
Serial.begin(115200);
while(!Serial){}
WiFiClient client;
const char* host="http://jsonplaceholder.typicode.com/";
String PostData = "title=foo&body=bar&userId=1";

if (client.connect(host, 80)) {

client.println("POST /posts HTTP/1.1");
client.println("Host: jsonplaceholder.typicode.com");
client.println("Cache-Control: no-cache");
client.println("Content-Type: application/x-www-form-urlencoded");
client.print("Content-Length: ");
client.println(PostData.length());
client.println();
client.println(PostData);

long interval = 2000;
unsigned long currentMillis = millis(), previousMillis = millis();

while(!client.available()){

  if( (currentMillis - previousMillis) > interval ){

    Serial.println("Timeout");
    blinkLed.detach();
    digitalWrite(2, LOW);
    client.stop();
    return;
  }
  currentMillis = millis();
}

while (client.connected())
{
  if ( client.available() )
  {
    char str=client.read();
   Serial.println(str);
  }
}
}
*/
