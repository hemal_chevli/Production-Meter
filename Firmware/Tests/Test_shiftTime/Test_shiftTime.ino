#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>

tmElements_t tmx;
//Use GMT time(2:30AM -> 8:00AM)

const int ShiftStartHour = 2;
const int ShiftStartMinute = 30 +5 ;
int Hour=0;
int NewHour=0;
//14:30 ->20:00(8:00PM)
const int ShiftEndHour = 14;
const int ShiftEndMinute = 30 + 5 ;
int shiftNow=0;
int preShift=0;
int eepromShift;
int eepromShiftAdd = 100;
//check eeprom write
void setup() {
  Serial.begin(115200);
  while (!Serial) ; // wait for serial
  delay(200);
  Serial.println("DS1307RTC Read");
  Serial.println("-------------------");
  RTC.read(tmx);
  printTime();
  if(ShiftStartHour <= tmx.Hour  && tmx.Hour <= ShiftEndHour && tmx.Minute >= ShiftEndMinute){
    if(shiftNow != 1){
      shiftNow = 1;
      Serial.print("Shift:");Serial.println(shiftNow);
    }
  }
  else{
    if(shiftNow != 2){
      shiftNow = 2;
      Serial.print("Shift:");Serial.println(shiftNow);
    }
  }

  //if shift has changed reset counter to zero
  if(shiftNow != preShift){
    Serial.println("---Shift changed");
    preShift = shiftNow;
  }
  
Serial.println("-------------------");
}
  
void loop() {
  for(int i=0; i<=24; i++){
    for(int j=0; j<=60; j++){

      if(ShiftStartHour <= i  && i < ShiftEndHour){
          if(shiftNow != 1 && j==ShiftEndMinute){
            shiftNow = 1;
            Serial.print("Shift:");Serial.println(shiftNow);
            Serial.print(i);Serial.print(":");Serial.println(j);
          }
        }
        else{
          if(shiftNow != 2 && j==ShiftEndMinute){
            shiftNow = 2;
            Serial.print("Shift:");Serial.println(shiftNow);
            Serial.print(i);Serial.print(":");Serial.println(j);
          }
        }

        //if shift has changed reset counter to zero
        if(shiftNow != preShift){
          Serial.print("---Shift changed to ");Serial.println(shiftNow);
          preShift = shiftNow;
        }

         NewHour = i;
        if(NewHour != Hour){
          Hour = NewHour;
          Serial.print("Counter Saved hour:"); //New line for eeprom msg
          Serial.println(Hour);
        }
      }
 }
while(1);
 if(Serial.available()){ //is p print time , s = set time
  // read the incoming byte:
  char incomingByte = Serial.read();
  if(incomingByte == 'p'){
    printTime();
  }
  if(incomingByte == 's'){
    tmx.Hour = 13;
    tmx.Minute = 59;
    tmx.Second = 55;

    tmx.Day = 5;
    tmx.Month =3;
    tmx.Year = CalendarYrToTm(2016);
    RTC.write(tmx); // sec, min, hr, day, mon, yr
    Serial.println("Time witten");
  }
 }
}
void print2digits(int number) {
  if (number >= 0 && number < 10) {
    Serial.write('0');
  }
  Serial.print(number);
}

 
 void printTime(void){
  if (RTC.read(tmx)) {
     Serial.print("Ok, Time = ");
     print2digits(tmx.Hour);
     Serial.write(':');
     print2digits(tmx.Minute);
     Serial.write(':');
     print2digits(tmx.Second);
     Serial.print(", Date (D/M/Y) = ");
     Serial.print(tmx.Day);
     Serial.write('/');
     Serial.print(tmx.Month);
     Serial.write('/');
     Serial.print(tmYearToCalendar(tmx.Year));
     Serial.println();
   }
 }